<?php
	header('Content-type: text/xml');
	header('Content-Disposition: attachment; filename="suggest.xml"');
		include_once('../../../wp-config.php');
		global $wpdb;
	
		$data = $wpdb->get_results("
			SELECT a.*,b.* ,DATE_FORMAT(a.add_date,'%d-%m-%Y') as pubdate
			FROM wp_kb_suggest a 
			left join
			(
				SELECT c.id_cat as parid, b.id_cat as subid,c.name as parcat,b.name as  subcat,b.description as subdes, c.description as pades
				FROM wp_kb_category b
				inner join wp_kb_category c on c.id_cat = b.parent_cat
			) b on a.id_cat = b.subid
			");
		
			$i=0;
			foreach ($data as $item) 
			{ 
				$i++;
				if($i==1)
				{
					echo '<?xml version="1.0" encoding="ISO-8859-1"?>
						      <LIBRARY>
							  	<COMMENT>Suggested Content</COMMENT>
								
								<BOOK name="'.$item->parcat.'" object_uid="'.$item->parid.'" caption="'.$item->parcat.'" tooltip=""
								<COMMENT>'.$item->pades.'</COMMENT>

								<CHAPTER name="'.$item->subcat.'" object_uid="'.$item->subid.'" caption="'.$item->subcat.'" tooltip="">
	      						<COMMENT>'.$item->subdes.'</COMMENT>';
				}
				echo '
					<PAGE name="'.$item->tittle.'" object_uid="'.md5($item->id_sug).'" caption="'.$item->tittle.'" 
						tooltip="'.$item->tags.'" author="'.$item->author.'" pubDate="'.RFC2822($item->pubdate).'">
				       	<COMMENT>'.$item->subcat.'</COMMENT>
	    		    	<LINK><PAGEURL>'.$item->link.'</PAGEURL></LINK>
		      		</PAGE>
				';
				
			}
			
			if($i>0)
			{
				echo '</CHAPTER></BOOK></LIBRARY>';
			}
			
			
		function RFC2822($date, $time = '00:00') 
		{
    		list($d, $m, $y) = explode('-', $date);
    		list($h, $i) = explode(':', $time);
		    return date('r', mktime($h,$i,0,$m,$d,$y));
		}
			
		
		
	
?>



