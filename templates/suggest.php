<?php $kbucket = KBucket::get_kbucket_instance(); ?>
<div class="container" style="margin-left:5px;">
	<p><label id="showmsg" style="color:red; width:100%; font-size:14px; text-align:center;">&nbsp;</label>
	<div id="kb-suggest-box">
		<form action="" method="post" id="suggform">
			<p><label>Category</label><span><?php $kbucket->render_categories_dropdown();?></span></p>
			<p><label for="stitle">Page Tittle* :</label><span><input type="text" name="stitle"  id="stitle" value="" /></span></p>
			<p><label for="stags">Tags* : </label><span><input type="text" name="stags" id="stags" value="" /></span></p>
			<p><label for="surl">Page URL* :</label><span><input type="text" name="surl" id="surl" value="" /></span></p>
			<p><label for="sauthor">Author* :</label><span><input type="text" name="sauthor" id="sauthor" value="" /></span></p>
			<p><label for="stwitter">Twitter Information page of author:</label><span><input type="text" id="stwitter" name="stwitter" value="" /></span></p>
			<p><label for="sfacebook">Facebook information page of author:</label><span><input type="text" id="sfacebook" name="sfacebook" value="" /></span></p>
			<p><label for="sdesc">Comment* : </label><span><textarea name="sdesc" id="sdesc"  ></textarea></span></p>
			<p><label>&nbsp;</label><span><input type="button" name="ssugg" value="Add Suggestion" onclick="validateSug
			()" /></span></p>
		</form>
	</div>
</div>