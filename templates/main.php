<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8" />
	<meta name="author" content="" />
	<title>Kbucket</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="<?php echo WPKB_PLUGIN_URL; ?>/css/bootstrap/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo WPKB_PLUGIN_URL; ?>/css/kbucket-main.css" type="text/css" />
</head>
<body>

<div id="home" class="home">
	<div class="container">
		<div class="row">
			<div id="kbucket-logo" class="text-center">
				<img src="<?php echo WPKB_PLUGIN_URL; ?>/images/KBUCKET-logo.png" alt="logo">
				<h2>Curate Your research.</h2>
				<h2> Earn Money</h2>
			</div>
		</div>
		<div class="row">
			<div class="">
				<div class="info-box-left">
					<p class="lead"> What is Curation?<br/>
						Curators is an editorial value creation activity!  Curators organize links to third party content in channels and tagging, making it easier for others to discover great content.
					</p>
					<div class="info-box-circle">
						Great editors are like great match makers: They introduce people to whole new ways.
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="">
				<div class="pull-right info-box-right">
					<p class="lead">What is KBucket?<br/>
						KBucket is a Plugin for WordPress websites that let’s publishers organize and publish hundreds of links on one page. KBucket page are organized into multiple channels and each link is tagged for easy access
					</p>
					<div class="info-box-circle">
						KBucket increases audience loyalty and engagement
					</div>
			</div>
		</div>

		<div class="row">
			<div class="">
				<div class="info-box-left">
					<p class="lead">KBucket for Content Marketing!<br/>
						Each link you curate can be used to drive traffic and generate leads to your site.  Creating new content is expensive, curating great content is not
					</p>
					<div class="info-box-circle">
						Marketers who share content drive traffic and gain customers
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="">
				<div class="pull-right info-box-right">
					<p class="lead">Earn money with KBucket!<br/>
						Start your KBucket topic and get the audience to contribute to your research! KBucket pages on average add 10 more pageviews per unique visitor and generate many returning visitors.  We will manage your ad revenue and help you make your KBucket page into a money maker!
					</p>
					<div class="info-box-circle">
						10X Pageviews per visitor.  Become a KBucketeer, join our ad network and we will monetize your traffic
					</div>
				</div>
			</div>
		</div>

	</div>
</div><!-- #body-core -->
<script src="<?php echo WPKB_PLUGIN_URL; ?>/js/jquery.js" type="text/javascript"></script>
<script src="<?php echo WPKB_PLUGIN_URL; ?>/css/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>