<?php
	if(isset($_REQUEST['c']))
	{
		include_once('../../../wp-config.php');
		global $wpdb;
		$cid = $_REQUEST['c'];
		
		$pid = $wpdb->get_var("select ID from wp_posts where post_content like '%[KBucket-Page]%' and post_status = 'publish' ");
		$url = get_permalink($pid).'?scategory='.$cid;
		//echo nl2br("select name,description,add_date from wp_kb_category where id_cat = '$cid' and status='1'");
		$catinfo = $wpdb->get_row("select name,description,add_date from wp_kb_category where id_cat = '$cid' and status='1'");
		
		$data = $wpdb->get_results("SELECT a.id_kbucket,id_cat,title,description,link,author,add_date,tags,pub_date
			FROM wp_kbucket a left join
			(
				SELECT a.id_kbucket,GROUP_CONCAT(name) as tags
				FROM wp_kbucket a
				Left Join wp_kb_tags b on b.id_kbucket  = a.id_kbucket
				Left Join wp_kb_tag_details c on c.id_tag = b.id_tag
				where a.id_cat = '$cid'
				group by a.id_kbucket 
			) b on a.id_kbucket = b.id_kbucket
			where a.id_cat = '$cid' ");
	}	
header("Content-type: text/xml"); ?>
<?php echo '<?xml version="1.0" encoding="iso-8859-1"?>';?>
<rss version="2.0">
<channel>
  <title><?php echo $catinfo->name;?></title>
  <link><?php echo $url;?></link>
  <description><?php echo $catinfo->description;?></description>
  <pubDate><?php echo $catinfo->add_date;?></pubDate>
<?php foreach ($data as $item) { ?>
  <item>
    <title><![CDATA[<?=$item->title;?>]]></title>
    <pubDate><![CDATA[<?=$item->pub_date;?>]]></pubDate>
	<author><![CDATA[<?=$item->author;?>]]></author>
    <link><![CDATA[<?=$item->link?>]]></link>
    <description><![CDATA[<?=$item->description?>]]></description>
	<category><![CDATA[<?=$item->tags?>]]></category>
  </item>
<?php } ?>
</channel>
</rss>