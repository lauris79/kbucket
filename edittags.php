<?php 


include_once("../../../wp-config.php");
global $wpdb;


extract($_REQUEST);
$tableTagDet = $wpdb->prefix."kbucket";
$sql = "SELECT author, author_alias FROM $tableTagDet WHERE author='$author' group by author, author_alias ";
$result= $wpdb->get_row($sql);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script src="<?php echo wpkb_plugin_url;?>/js/jquery.js" type="text/javascript"></script>
<script src="<?php echo wpkb_plugin_url;?>/js/jquery.validate.js" type="text/javascript"></script>
<script>
	$(document).ready(function(){
		$("#submit").click(function(){
			var val = $("#tag_name").val();
			if(val==""){	alert("Alias Name cannot be empty");	return false;	}
		});
	});
</script>
</head>

<body>
<form method="post" name="formEdit" id="formEdit">
<table width="350" cellpadding="0" cellspacing="0">
	
	<?php
	if(isset($_REQUEST['edit']))
	{
	?>
	<tr>
    	<th colspan="2" align="center" height="50">Update Tag Details</th>
    </tr>
	<tr>
    	<td width="50%"> Author</td>
        <td width="50%" height="40">
        	<?php echo $result->author;?>
			<input type="hidden" name="author" value="<?php echo $result->author;?>" />
        </td>
    </tr>
	<tr>
    	<td width="50%">Author Alias</td>
        <td width="50%" height="40">
        	<input type="text" name="tag_name" id="tag_name" value="<?php echo $result->author_alias;?>" class="required" />
        </td>
    </tr>
    <tr>
    	<td></td><td><input type="submit" name="submit" id="submit" value="Update Tag" /></td>
    </tr>
	<?php
	}
	else if(isset($_REQUEST['delete']))
	{
	?>
		<tr>
    		<th colspan="2" style="padding:25px;"> Are you sure to want to delete "<?php echo $result->author;?>" KBucket Data</th>
			
    	</tr>
	
		<tr>
    		<td colspan="2" align="center">
				<input type="hidden" name="author" value="<?php echo $result->author;?>" />
				<input type="submit" name="submit" id="submit" value="Delete Author" />
			</td>
    	</tr>
	<?php
	}
	?>
</table>

</form>
</body>
</html>