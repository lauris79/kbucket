jQuery.noConflict();

function confirmdelete(x) {
    var r = confirm("Are you sure to want to delete?");
    if (r == true) {
        self.location = ajaxObj.adminUrl + "?page=KBucket&delsug=" + x;
    }
}
function kbucketRowCallback(row, data, index)
{
    var link = '<a href="' + ajaxObj.kbucketUrl + '/kb/' + data.id_kbucket + '">' + data.title + '</a>',
        isSticky = '<input type="checkbox" name="post_id" id="checkbox-sticky-' + data.id_kbucket  + '" class="checkbox-sticky"' + (data.post_id !== null ? ' checked="checked"' : '') + '/>';
    //alert(link);
    jQuery("td", row).eq(1).html(link);
    jQuery("td", row).eq(5).html(isSticky);
    if (data.image_url !== '') {
        var image = '<span id="kb-image-' + data.id_kbucket + '"><img src="' + data.image_url + '" alt="." style="width:80px;height:80px"/></span>';
        jQuery("td", row).eq(4).html(image);
    }
}

jQuery(document).ready(
    function () {
        
        jQuery("#form").validate();
    
        jQuery(".colorpickerField").ColorPicker({
            onSubmit: function (hsb, hex, rgb, el)
            {
                jQuery(el).val('#' + hex);
                jQuery(el).ColorPickerHide();
            },
            onBeforeShow:
                function () {
                    jQuery(this).ColorPickerSetColor(this.value);
                }
        });
    
        if (jQuery('#kBucketTabs').length > 0) {
            var countries = new ddtabcontent("kBucketTabs");
            countries.setpersist(true);
            countries.setselectedClassTarget("link"); //"link" or "linkparent"
            countries.init();
        }

        jQuery("#category-dropdown:first-child").attr('selected', 'selected');
        jQuery("#subcategory-dropdown:first-child").attr('selected', 'selected');
        jQuery("#subcategory-dropdown").attr('disabled', 'disabled');

        jQuery("#category-dropdown").on("change", function(){
            var categoryId = jQuery(this).val();
            if (categoryId == '') {
                return false;
            }
            jQuery("#subcategory-dropdown").load(ajaxurl, {"action" : "get_subcategories", "category_id" : categoryId},function(response){
                jQuery("#subcategory-dropdown").removeAttr('disabled');
            });
        });

        jQuery("#subcategory-dropdown").on("change", function(){
            var categoryId = jQuery(this).val();
            if (categoryId == '') {
                return false;
            }
            
            jQuery("#kbuckets").html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="kbuckets-list"></table>');
            var options =  {
                "dataType" : "json",
                "ajax": {
                    "type" : "POST",
                    "url" : ajaxurl, 
                    "data" : {action : "get_kbuckets", category_id : categoryId}
                },
                "columns":[
                    {"mData":"name","sTitle":"Category"},
                    {"mData":"title","sTitle":"Title"},
                    {"mData":"author","sTitle":"Author"},
                    {"mData":"pub_date","sTitle":"Published at"},
                    {"mData":"image_url","sTitle":"Image"},
                    {"mData":"post_id","sTitle":"Sticky"}],
                "bDestroy":true,
                "bFilter" : false,
                "pageLength":200,
                "bLengthChange": false,
                "createdRow":function(row, data, index) {
                    kbucketRowCallback(row, data, index);
                },
                "initComplete": function () {
                    
                    var  kbOn = [],
                        kbOff = [];

                    jQuery("#button-save-sticky").show();
                    
                    jQuery('.checkbox-sticky').on("click", function () {
                        var id = jQuery(this).attr('id').replace('checkbox-sticky-', ''),
                            state = jQuery(this).is(':checked');
                        if (state == true) {
                            valueIndex = jQuery.inArray( id, kbOff );
                            if ( valueIndex !== '-1' ) {
                                //alert(valueIndex );
                                kbOff.splice(kbOff.indexOf(valueIndex), 1);
                            }
                            kbOn.push(id);
                        } else {
                            valueIndex = jQuery.inArray( id, kbOn );
                            if ( valueIndex !== '-1') {
                                kbOn.splice(kbOn.indexOf(valueIndex), 1);
                            }
                            kbOff.push(id);
                        }
                    });
                    
                    jQuery("#button-save-sticky").on("click", function () {
                        
                        jQuery("#messages").empty();

                        if (kbOn.length == 0 && kbOff.length == 0) {
                            return;
                        }

                        jQuery.ajax({
                            url: ajaxurl,
                            type: "POST",
                            data: {action : "save_sticky", kb_on : kbOn, kb_off : kbOff},
                            success: function (res) {
                                kbOn = [];
                                kbOff = [];
                                stickySavedResponse(res);
                                if (typeof res.images !== undefined) {
                                    jQuery.each(res.images, function(kbId, imgUrl){
                                        jQuery("#kb-image-" + kbId).html('<img src="' + imgUrl + '" alt="." style="width:80px;height:80px"/>');
                                    });
                                }
                            }
                        });
                    });
                }
            };

            var dataTable = jQuery('#kbuckets-list').DataTable(options);
        });
});

function stickySavedResponse(res)
{
    var msg = (res.status == 'ok' ? 'Changes has been saved successfully!' : 'There was an error.'),
        msgClass = (res.status == 'ok' ? 'updated-success' : 'updated-error');
        
    jQuery('#messages').html('<span class="' + msgClass + '">' + msg + '</span>');
    
}