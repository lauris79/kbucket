var $kbj = jQuery;

function scrollToElement( target ) {
    var topoffset = 30;
    var speed = 800;
    var destination = $kbj( target ).offset().top - topoffset;
    $kbj( 'html:not(:animated),body:not(:animated)' ).animate( { scrollTop: destination}, speed, function() {
        window.location.hash = target;
    });
    return false;
}

function moveSuggestedItem(event, ui) {
    return '<div id="draggableHelper">Drag me into "Suggest Content"</div>';
}

function openSuggestBox(e, obj)
{
    e.preventDefault();
    var url = obj.attr('href'),
        title = obj.attr('title');
    $kbj.facebox(function() {
        $kbj.get(url, function(data) { $kbj.facebox(data) })
    });
}

function fillSuggestContent(event, ui) {
    var obj = ui.draggable,
        link = $kbj(obj).find('.kb-link'),
        title = link.text(),
        surl = link.attr('href'),
        tags = $kbj(obj).find('.kb-tag-link').text(),
        author = $kbj(obj).find('.kb-item-author-name').text();

        $kbj.facebox(function() {
            var url = $kbj("#kb-suggest").attr('href');
            $kbj.get(url, function(data) { 
                $kbj.facebox(data);
                $kbj("#sid_cat").val(kbObj.categoryName);
                $kbj("#stitle").val(title);
                $kbj("#stags").val(tags);
                $kbj("#surl").val(surl);
                $kbj("#sauthor").val(author);
            })
        });
}

function shareBox(id) {
    $kbj.facebox(function() {
        $kbj.get(kbObj.ajaxurl, {"action" : "share_content", "kb-share" : id}, function(data) {
            $kbj.facebox(data);
            refreshAddthis();
            if (typeof window.adsbygoogle !== 'undefined') {
                (adsbygoogle = window.adsbygoogle || []).push({});
            }
         });
    });
    return false;
}


function addthisEventHandler(evt)
{ console.log(evt);
    switch (evt.type) {
        case "addthis.menu.open":
            addThisOpen(evt);
        break;
        case "addthis.menu.close":
            addThisClose(evt);
        break;
        case "addthis.menu.share":
            addThisShare(evt);
        break;
        default:
        console.log('received an unexpected event', evt);
    }
}

function addThisOpen(evt) {
    console.log(evt);
}

function addThisClose(evt) {
    //alert('close');
    
}

function addThisShare(evt) {
    //alert('share');
}

function refreshAddthis() {
    if (typeof window.addthis == 'undefined' || typeof window.addthis.toolbox == 'undefined') {
        return false;
    }
    console.info('refreshing');
    window.addthis.toolbox('.addthis_toolbox');
}

function validateSug()
{
    if(($kbj('#stitle').val()) == "" )
        $kbj('#showmsg').text("Please enter Page Tittle");
    else if(($kbj('#stags').val()) == "" )
        $kbj('#showmsg').text("Please enter Page Tags");
    else if(($kbj('#surl').val()) == "" )
        $kbj('#showmsg').text("Please enter Page URL");
    else if(($kbj('#sauthor').val()) == "" )
        $kbj('#showmsg').text("Please enter Page Author");
    else
    {
        $kbj.ajax({
            type: 'POST',
            url: kbObj.kbucketUrl + "/ajax.php",
            data:
            {
                sid_cat: $kbj('#sid_cat').val(),
                stitle: $kbj('#stitle').val(),
                stags: $kbj('#stags').val(),
                surl: $kbj('#surl').val(),
                sauthor: $kbj('#sauthor').val(),
                stwitter: $kbj('#stwitter').val(),
                sfacebook: $kbj('#sfacebook').val(),
                sdesc: $kbj('#sdesc').val()
            },
            success: function(){
                $kbj('#kb-suggest-box').empty();
                $kbj('#showmsg').text("Your suggestion was submitted successfully.");
                $kbj('#showmsg').css("color","green");
                document.getElementById('suggform').reset();
            },
            error: function(){
                alert('failure');
            }
        });
    }
}


$kbj(document).ready(function ($kbj) {
        
        $kbj("#kb-search-button").on("click", function(){
            $kbj("#kb-search").show();
        });
        
        if (typeof kbSearch !== "undefined") {
            
            $kbj('body').find('input[name="s"]').each(
                function () {
                    $kbj(this).attr('name', 'srch');
                    $kbj(this).val(searchRes);
                    $kbj(this.form).attr('action', permalink);
                }
            );
        }

        if (typeof kbObj == "undefined") {
            return false;
        }
        
        if (typeof $kbj.facebox !== "undefined") {
            
            $kbj(document).bind('beforeReveal.facebox', function() {
                $kbj('#facebox .content').width('720px');
            });
            
            $kbj.facebox.settings.closeImage = kbObj.kbucketUrl + '/images/closelabel.png';
            $kbj.facebox.settings.loadingImage = kbObj.kbucketUrl + '/images/loading.gif';
        }
        
        if (typeof kbObj.shareId !== "undefined") {
            shareBox(kbObj.shareId);
            delete (kbObj.shareId);
        }
        
                
        $kbj(".kb-toggle").on("click", function(){
            var i = $kbj(this).attr("id").replace("kb-toggle-","");
            if ($kbj(this).text() == '[+]') {
                $kbj("#kb-item-text-" + i).show();
                $kbj(this).text('[-]');
            } else {
                $kbj("#kb-item-text-" + i).hide();
                $kbj(this).text('[+]');
            }
        });
        
        $kbj(".kb-share-item").on("click", function(e){
            e.preventDefault();
            
            var id = $kbj(this).attr('id').replace("kb-share-item-", "");

            if (typeof $kbj.facebox.settings !== "undefined") {
                $kbj(document).bind('afterClose.facebox', function(){
                    refreshAddthis();
                    scrollToElement("#kb-item-" + id);
                });
            }

            shareBox(id);
        });

        $kbj(".kb-suggest").on("click", function(e){
            openSuggestBox(e, $kbj(this));
        });

        $kbj(".kb-item" ).draggable({
                cursor: 'move',
                containment:"document",
                revert: true,
                helper:"clone",
                cursorAt: { right: 500 }
            });
        
        $kbj("#kb-suggest").droppable({
            activeClass: "ui-state-hover",
            hoverClass: "ui-state-active",
            tolerance: "touch", 
            drop: fillSuggestContent
        });
    }
);