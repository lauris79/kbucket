<?php
/*
Plugin Name: KBucket
Plugin URI: http://optimalaccess.com/content/kbucket-wordpress-plugin
Description: KBucket Curated Pages
Version: 2.0
Author: Optimal Access Inc.
Author URI: http://optimalaccess.com/
License: KBucket
*/

// Set Kbucket install hook.
register_activation_hook( __FILE__, 'register_activation_kbucket' );

/**
 * Kbucket uninstall hook
 */
//register_uninstall_hook( __FILE__, 'register_deactivation_kbucket' );
//register_deactivation_hook( __FILE__, 'register_deactivation_kbucket' );


// Start KBucket from init hook
add_action( 'init', 'action_set_kbucket' );

// Add custom rewrite rules for SEO links
add_action( 'generate_rewrite_rules', 'rewrite_rules' );

function custom_excerpt_length() {

	return 2;
}

function read_more_link() {
	global $post;
	return '<a class="moretag" href="'. get_permalink( $post->ID ) . '">More</a>';
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
//add_filter( 'the_excerpt', 'themeprefix_excerpt_read_more_link' );
add_filter( 'excerpt_more', 'read_more_link' );

// Disable search query
//add_action( 'parse_query', 'fb_filter_query' );

// Disable wp search box
//add_filter( 'get_search_form', function() { return null; } );

/**
 * Set separate action hooks for admin and site
 * Fired by wp action hook: init
 */
function action_set_kbucket()
{
	/** @var $wp_rewrite object */
	global $wp_rewrite;

	$wp_rewrite->flush_rules();

	// Get Kbucket instance
	$kbucket = KBucket::get_kbucket_instance();

	// Set AJAX action hook for share content box
	add_action( 'wp_ajax_nopriv_share_content', array( $kbucket, 'action_ajax_share_content' ) );

	// Set admin hooks for dashboard
	if ( is_admin() ) {
		add_action( 'admin_menu', array( $kbucket, 'action_admin_menu' ) );
		add_action( 'admin_enqueue_scripts', array( $kbucket, 'action_admin_enqueue_scripts' ) );
		add_action( 'wp_ajax_save_sticky', array( $kbucket, 'action_ajax_save_sticky' ) );
		add_action( 'wp_ajax_get_subcategories', array( $kbucket, 'action_ajax_get_subcategories' ) );
		add_action( 'wp_ajax_get_kbuckets', array( $kbucket, 'action_ajax_get_kbuckets' ) );
		add_action( 'wp_ajax_share_content', array( $kbucket, 'action_ajax_share_content' ) );
		add_filter( 'post_link', array( $kbucket, 'filter_post_link' ), 10, 3 );

		// If sticky post has been updated, we can update kbucket too. Not in use in this version
		//add_action( 'edit_post', array( $kbucket, 'action_update_kbucket_from_sticky' ), 10, 3 );
		return true;
	}

	// Init Kbucket when wp object is initialized
	add_action( 'wp', 'action_init_kbucket' );

	// Set action for custom search query
	//add_action( 'pre_get_posts', array( $kbucket, 'customize_search_query' ) );

	//Customize search query to join items from Kbucket table / @TODO: Move kbuckets from separate table to custom posts by default
	add_filter( 'posts_join', array( $kbucket, 'kb_search_join' ) );
	
	// Customize search query to get Kbuckets
	add_filter( 'posts_where', array( $kbucket, 'kb_search_query' ) );

	return true;
}

/**
 * Kbucket initialization
 * Set action hooks,filters and templates
 * Fired by wp action hook: wp
 * @return bool
 */
function action_init_kbucket() {

	// Return Kbucket instance
	$kbucket = KBucket::get_kbucket_instance();
	
	// Set callback method for <head> content
	add_filter( 'wp_head', array( $kbucket, 'filter_wp_head' ), 9 );

	// Set callback method for post content
	add_filter( 'the_content', array( $kbucket, 'filter_the_content' ) );

	// Set callback method for footer content
	add_filter( 'wp_footer', array( $kbucket, 'filter_wp_footer' ) );

	// Evil behaviour. Remove canonical Meta tag from <head> to avoid problems with share scripts and other plugins
	remove_action( 'wp_head', 'rel_canonical' );

	// Set post link callback to point posts to Kbucket listing page with share window
	add_filter( 'post_link', array( $kbucket, 'filter_post_link' ), 10, 2 );

	// Set page title filter
	//add_filter( 'wp_title', array( $kbucket, 'filter_the_title' ) );

	// Set shortcode for "Suggest Page" Button
	add_shortcode( 'suggest-page', array( $kbucket, 'shortcode_render_suggest_page' ) );

	// Write settings and page data to Kbucket Page object
	$kbucket->init_kbucket_section();

	if ( $kbucket->isKbucketPage ) {

		// Detect mobile devices and set the property
		$kbucket->is_mobile();

		add_filter( 'jetpack_enable_opengraph', '__return_false', 99 );
	}

	// Set custom template
	add_action( 'page_template', array( $kbucket, 'action_set_page_template' ) );

	// Initialize scripts
	add_action( 'wp_enqueue_scripts', array( $kbucket, 'action_init_scripts' ) );

	return true;
}

// Define site URL
define( 'WPKB_SITE_URL', home_url() );

// Define Kbucket listing page URL
define( 'KBUCKET_URL', WPKB_SITE_URL . '/kbucket' );


/**
 * Class KBucket
 */
class KBucket
{
	public $template = '';
	public $isKbucketPage = false;

	/** @var $wpdb object */
	public $wpdb;

	public static $instance = null;

	/** @var $kbPage object */
	private $kbPage;

	/** @var $kbucket object */
	private $kbucket = false;

	/** @var $category object */
	private $category = false;

	private $isMobile = false;
	private $kbucketCurrentUrl = '';
	private $metaOgUrl = '';
	private $addthisId = 'ra-54aacc3842e62476'; //@TODO avoid harcoding - should be built according to settings

	// Optimalaccess site
	
	private $googleadsClient = 'ca-pub-0683712003699658';
	private $googleadsSlot = '2562659265';
	private $googleadsSlotMobile = '7132459662';
	/*
	// Eontransform site
	private $googleadsClient = 'ca-pub-0683712003699658';
	private $googleadsSlot = '1085926068';
	private $googleadsSlotMobile = '4178993264';*/


	private $disqusConfig = array(
		'shortname' => '',
		'title' => 'Kbuckets',
		'url' => KBUCKET_URL,
		'id' => 0,
	);

	// List of available share buttons in Addthis toolbox
	private $addthisShareButtons = array(
				'facebook' => '',
				'twitter' => '',
				'digg' => '',
				'email' => '',
				'pinterest_share' => '',
				'stumbleupon' => '',
				'linkedin' => '',
				'google_plusone' => array( 'g:plusone:count' => 'false', 'g:plusone:size' => 'medium' ),
			);

	/**
	 * Class Constructor
	 */
	public function __construct()
	{
		// Define plugin path
		define( 'WPKB_PATH',  plugin_dir_path( __FILE__ ) );

		// Define plugin URL
		define( 'WPKB_PLUGIN_URL', plugins_url() . '/KBucket' );

		global $wpdb;

		// Encapsulate wpdb object to avoid global variables
		$this->wpdb = $wpdb;

		// Kbucket data object
		$this->kbPage = new stdClass();

		// Read Plugin settings from database
		$this->kbPage->settings = $this->wpdb->get_row( 'SELECT * FROM `' . $this->wpdb->prefix . 'kb_page_settings`' );

		// Set custom template property for section
		// @TODO - templates are not needed anymore. Should be changed to ajax actions
		if ( ! empty( $_GET['kbt'] ) ) {
			$template = esc_attr( $_GET['kbt'] );
			$this->template = substr( $template, 0, 10 ) . '.php';
		}

		// Set Disqus shortname if "Disqus Comment System" plugin detected
		if ( defined( 'DISQUS_DOMAIN' ) ) {
			$this->disqusConfig['shortname'] = strtolower( get_option( 'disqus_forum_url' ) );
		}

		return true;
	}

	/*****************************
	 * AJAX actions
	 *****************************
	 */

	/**
	 * Render dropdown with subcategories by category id
	 * Requires POST param category_id
	 * Fired by wp ajax hook: wp_ajax_get_subcategories
	 */
	public function action_ajax_get_subcategories()
	{
		if ( empty( $_POST['category_id'] ) ) {
			return;
		}

		$sql = 'SELECT `id_cat`, `name`
				FROM `' . $this->wpdb->prefix . 'kb_category`
				WHERE `parent_cat`=%s';

		$categoryId = sanitize_text_field( $_POST['category_id'] );

		$query = $this->wpdb->prepare( $sql, $categoryId );

		$subcategories = $this->wpdb->get_results( $query );

		$response = '<option value="">Select Subcategory</option>';
		foreach ( $subcategories as $s ) {
			$response .= '<option value="' . esc_attr( $s->id_cat ) . '">' . esc_html( $s->name ) . '</option>';
		}

		echo $response;
		exit;
	}

	/**
	 * Render DataTables compatible JSON object containing Kbuckets by category id
	 * Requires POST param category_id
	 * Fired by wp ajax hook: wp_ajax_get_kbuckets
	 */
	public function action_ajax_get_kbuckets()
	{
		if ( empty( $_POST['category_id'] ) ) {
			return;
		}

		header( 'Content-Type: application/json' );

		$sql = "
				SELECT c.name,
						k.id_kbucket,
						k.title,
						k.url_kbucket AS link,
						k.author,
						k.`pub_date`,
						STR_TO_DATE((SUBSTR(`pub_date`, 5, 12) ), '%%d %%b %%Y' ) AS add_date,
						k.image_url,
						k.post_id
				FROM `" . $this->wpdb->prefix .  'kbucket` k
				LEFT JOIN `' . $this->wpdb->prefix .  'kb_category` c
					ON k.id_cat=c.id_cat
				WHERE c.`id_cat`=%s
				ORDER BY add_date DESC
				LIMIT 20';

		$categoryId = esc_sql( $_POST['category_id'] );

		$query = $this->wpdb->prepare( $sql, $categoryId );
		$kbuckets = $this->wpdb->get_results( $query );

		echo json_encode( array( 'data' => $kbuckets ) );
		exit;
	}

	/**
	 * Render share window content
	 * Requires POST param kb-share
	 * Fired by wp ajax hook: wp_ajax_nopriv_share_content
	 */
	public function action_ajax_share_content()
	{
		if ( empty( $_GET['kb-share'] ) ) {
			return false;
		}

		header( 'Cache-Control: no-store, no-cache, must-revalidate, max-age=0' );

		$kid = esc_attr( $_GET['kb-share'] );

		$kbucket = $this->get_kbucket_by_id( $kid );

		if ( ! $kbucket ) {
			return false;
		}

		$imageUrl = $kbucket->image_url !== '' ? $kbucket->image_url : false;

		// Scrape article image from the source site,if the image URL not in the database
		if ( ! $imageUrl ) {
			$imageUrl = $this->scrape_og_image( $kbucket->link );

			// Set the the image from Kbucket category if scraping was not sucessful
			if ( ! $imageUrl ) {

				$query = $this->wpdb->prepare(
				 'SELECT `image` FROM `' . $this->wpdb->prefix . 'kb_category`
				 WHERE `id_cat`=%s',
				 $kbucket->id_cat
				);

				$categoryImage = $this->wpdb->get_col( $query );

				if ( ! empty( $categoryImage[0] )
					&& file_exists( WPKB_PATH . '/uploads/categories/' . $categoryImage[0] ) ) {
					$imageUrl = WPKB_PLUGIN_URL . '/uploads/categories/' . $categoryImage[0];
				}
			}

			// Save image url to database
			if ( $imageUrl ) {
				$this->wpdb->update(
				 $this->wpdb->prefix . 'kbucket',
				 array(
					'image_url' => $imageUrl,
				 ),
				 array( 'id_kbucket' => $kbucket->id_kbucket ),
				 array( '%s' ),
				 array( '%s' )
				);
				$kbucket->image_url = $imageUrl;
			}
		}

		if ( empty( $kbucket->post_id ) ) {
			$shareData = $this->get_kbucket_share_data( $kbucket );
		} else {

			$post = get_post( $kbucket->post_id );

			// Get sharing data
			$shareData = array(
				'url' => $kbucket->url_kbucket,
				'imageUrl' => $kbucket->image_url,
				'title' => $post->post_title,
				'description' => $post->post_content,
			);
		}

		$url = WPKB_SITE_URL . '/' . $shareData['url']; ?>
		<div class="kb-wrapper">
			<div id="kb-share-box" class="kb-share-box">
				<?php
				$this->render_share_toolbox(
				 array(
					'url' => $url,
					'title' => $shareData['title'],
					'image' => $shareData['imageUrl'],
					'description' => $shareData['description'],
					)
				); ?>
			</div>
			<div class="kb-item">
				<h3><a href="<?php echo esc_attr( $kbucket->link ); ?>" target="_blank"><?php echo esc_html( $shareData['title'] );?></a></h3>
				<?php if ( ! empty( $shareData['imageUrl'] ) ): ?>
					<img src="<?php echo esc_attr( $shareData['imageUrl'] ); ?>" class="share-image" style="width:200px;height:120px"/><br/>
				<?php endif; ?>
				<span style="color:<?php echo esc_attr( $this->kbPage->settings->author_color ); ?> !important;">Author: <?php echo esc_html( $kbucket->author ); ?></span>
				<span class="kb-item-date" style="color:<?php echo esc_attr( $this->kbPage->settings->author_color ); ?> !important;"><?php echo esc_html( $kbucket->add_date ); ?></span>
				<p style="color:<?php echo esc_attr( $this->kbPage->settings->content_color ); ?> !important;"><?php
					echo wp_kses(
					 $shareData['description'],
					 array(
						'a' => array(
							'href' => array(),
							'title' => array()
						),
						'br' => array(),
					 )
					); ?></p>
				<p class="blue" style=" margin-bottom:2px;">
					<span style="color:<?php echo esc_attr( $this->kbPage->settings->tag_color ); ?> !important;">Tags: <?php echo esc_html( $kbucket->tags ); ?></span>
				</p>
			</div>
			<div id="disqus_thread"></div>
			<?php /*
			<script type="text/javascript">
				if (typeof DISQUS !== 'undefined') {
					DISQUS.reset({
						reload: true,
						config: function () {
							this.page.identifier = '<?php echo $kbucket->id_kbucket; ?>';
							this.page.url = '<?php echo $url; ?>';
							this.page.title = '<?php echo $shareData['title']; ?>';
						}
					});
				}
			</script>
			<?php
			$this->render_disqus_config(
			 $this->disqusConfig['shortname'],
			 $shareData['title'],
			 $url,
			 $kbucket->id_kbucket
			);*/
			//wp_enqueue_script( 'disqus_embed', 'http://' . $this->disqusConfig['shortname'] . '.disqus.com/embed.js' );
			//wp_enqueue_script( 'kb-share-google_ads', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js' );
			?>
			<div id="ads-section">
				<?php echo $this->kbPage->settings->advertisiment_code; ?>
			</div>
		</div>
		<?php
		exit;
	}

	/**
	 * Insert Sticky Kbuckets as WP posts and return JSON result response
	 * Requires one of POST params kb_on | kb_off
	 * Fired by wp ajax hook: wp_ajax_save_sticky
	 */
	public function action_ajax_save_sticky()
	{
		if (
			( empty( $_POST['kb_on'] ) || ! is_array( $_POST['kb_on'] ) )
			&& ( empty( $_POST['kb_off'] ) || ! is_array( $_POST['kb_off'] ) )
		) {
			return;
		}

		header( 'Content-Type: application/json' );

		// Posts to write
		if ( ! empty( $_POST['kb_on'] ) ) {

			$args = array( 'hide_empty' => 0 );

			$wpCategories = array();
			$wpc = get_categories( $args );
			foreach ( $wpc as $cat ) {
				$wpCategories[ $cat->name ] = $cat->cat_ID;
			}

			$where = "a.`id_kbucket` IN ( '" . implode( "','", $_POST['kb_on'] ) . "' )";

			$kbuckets = $this->get_kbuckets( $where );

			$savedImages = array();

			foreach ( $kbuckets as $kbucket ) {

				if ( $kbucket->post_id > 0 ) {
					continue;
				}

				$content = $kbucket->description;

				$postData = array(
					'post_content' => $content,
					'post_title' => $kbucket->title,
					'post_status' => 'publish',
					'post_author' => $kbucket->author,
					'post_date' => $kbucket->add_date,
					'post_date_gmt' => $kbucket->add_date,
					'comment_status' => 'closed',
					'post_category' => array( $wpCategories[ $kbucket->categoryName ] )
				);

				/** @var $postId object|int */
				$postId = wp_insert_post( $postData, true );

				// Wpdb error object returned if there was an error
				if ( is_object( $postId ) ) {
					echo json_encode( array( 'status' => $postId->last_error ) );
					exit;
				}

				add_post_meta( $postId, '_url_kbucket', $kbucket->url_kbucket, true );
				add_post_meta( $postId, '_id_kbucket', $kbucket->id_kbucket, true );

				$updateArr = array( 'post_id' => $postId );
				$updatePlaceholders = array( '%d' );

				// Get image url from site og:image meta tag
				$imageUrl = $this->scrape_og_image( $kbucket->link );

				if ( $imageUrl !== '' ) {

					$attachment = $this->save_post_image( $imageUrl, $postId, $kbucket->title );

					if ( empty( $attachment['error'] ) && ! empty( $attachment['url'] ) ) {

						$imageFile = $attachment['file'];

						$filetype = wp_check_filetype( basename( $imageFile ), null );

						$postData = array(
							'post_mime_type' => $filetype['type'],
							'post_title' => $kbucket->title,
							'post_content' => '',
							'post_status' => 'inherit',
						);

						$attachmentId = wp_insert_attachment( $postData, $imageFile, $postId );

						if ( ! function_exists( 'wp_generate_attachment_data' ) ) {
							require_once( ABSPATH . 'wp-admin' . '/includes/image.php' );
						}

						$attachmentData = wp_generate_attachment_metadata( $attachmentId, $imageFile );
						if ( wp_update_attachment_metadata( $attachmentId,  $attachmentData ) ) {
							add_post_meta( $postId, '_thumbnail_id', $attachmentId, true );
						}

						$savedImages[ $kbucket->id_kbucket ] = $imageUrl;

						$updateArr['image_url'] = $imageUrl;
						$updatePlaceholders[] = '%s';
					}
				}
				$this->wpdb->update(
				 $this->wpdb->prefix . 'kbucket',
				 $updateArr,
				 array( 'id_kbucket' => $kbucket->id_kbucket ),
				 $updatePlaceholders,
				 '%s'
				);
			}
		}

		// Posts to delete
		if ( ! empty( $_POST['kb_off'] ) ) {

			$where = "a.`id_kbucket` IN ( '" . implode( "','", $_POST['kb_off'] ) . "' )";

			$kbuckets = $this->get_kbuckets( $where );

			foreach ( $kbuckets as $kbucket ) {

				if ( ! $kbucket->post_id > 0 ) {
					continue;
				}

				$args = array(
					'post_type' => 'attachment',
					'post_status' => 'any',
					'posts_per_page' => -1,
					'post_parent' => $kbucket->post_id,
				);
				$attachments = new WP_Query( $args );
				$attachment_ids = array();
				if ( $attachments->have_posts() ) {
					while ( $attachments->have_posts() ) {
						$attachments->the_post();
						$attachment_ids[] = get_the_id();
					}
				}

				wp_reset_postdata();

				if ( ! empty( $attachment_ids ) ) {
					$delete_attachments_query = $this->wpdb->prepare(
					 'DELETE FROM %1$s WHERE %1$s.ID IN (%2$s)',
					 $this->wpdb->posts,
					 join( ',', $attachment_ids )
					);
					$this->wpdb->query( $delete_attachments_query );
				}

				if ( false === wp_delete_post( $kbucket->post_id, true )  ) {
					continue;
				}
			}

			$this->wpdb->query(
			 'UPDATE `' . $this->wpdb->prefix . "kbucket`
			 SET `post_id`=NULL
			 WHERE `id_kbucket`
				IN ( '" . implode( "','", $_POST['kb_off'] ) . "' )"
			);
		}

		if ( $this->wpdb->last_error !== '' ) {
			echo json_encode( array( 'status' => $this->wpdb->last_error ) );
			exit;
		}

		$response = array( 'status' => 'ok' );

		if ( ! empty( $savedImages ) ) {
			$response['images'] = $savedImages;
		}

		echo json_encode( $response );
		exit;
	}

	/*****************************
	 * Actions fired by Hooks
	 *****************************
	 */

	/**
	 * Set frontend styles and scripts
	 * Fired by wp action hook: wp_enqueue_scripts
	 */
	public function action_init_scripts()
	{
		// Invoke jQuery mobile for mobile version
		if ( $this->isKbucketPage && $this->isMobile ) {

			global $wp_styles;

			// Reset theme css
			$wp_styles = array();

			global $wp_scripts;

			// Reset all theme scripts
			$wp_scripts = array();

			wp_enqueue_style( 'jquery-mobile-css', WPKB_PLUGIN_URL .'/js/jquery-mobile/jquery.mobile-1.4.5.min.css' );
			wp_enqueue_style(
			 'jquery-mobile-structure-css',
			 WPKB_PLUGIN_URL .'/js/jquery-mobile/jquery.mobile.structure-1.4.5.min.css'
			);
			wp_enqueue_style(
			 'jquery-mobile-theme-css',
			 WPKB_PLUGIN_URL .'/js/jquery-mobile/jquery.mobile.theme-1.4.5.min.css'
			);
			wp_enqueue_style( 'kbucket-mobile-css', WPKB_PLUGIN_URL .'/css/style-mobile.css' );

			wp_enqueue_script(
			 'jquery-mobile-js',
			 WPKB_PLUGIN_URL . '/js/jquery-mobile/jquery.mobile-1.4.5.js',
			 array( 'jquery' )
			);

			wp_enqueue_script(
			 'kbucket-js',
			 WPKB_PLUGIN_URL . '/js/kbucket.js'
			);

			return;
		}

		wp_enqueue_style( 'kbucket-facebox-css', WPKB_PLUGIN_URL . '/js/facebox/src/facebox.css' );
		wp_enqueue_style( 'kbucket-css', WPKB_PLUGIN_URL .'/css/kbucket-style.css', array( 'kbucket-facebox-css' ) );

		// Register Facebox modal window
		wp_register_script(
		 'facebox-js',
		 WPKB_PLUGIN_URL . '/js/facebox/src/facebox.js',
		 array( 'jquery' ),
		 '',
		 true
		);

		// Register Kbucket scripts
		wp_register_script(
		 'kbucket-js',
		 WPKB_PLUGIN_URL . '/js/kbucket.js',
		 array( 'jquery', 'facebox-js', 'jquery-ui-core', 'jquery-ui-draggable', 'jquery-ui-droppable' ),
		 '',
		 true
		);

		wp_enqueue_script( 'facebox-js' );
		wp_enqueue_script( 'kbucket-js' );
	}

	/**
	 * Set admin styles and scripts
	 * Fired by wp action hook: admin_enqueue_scripts
	 */
	public function action_admin_enqueue_scripts()
	{
		wp_enqueue_style( 'tabcontent-css', WPKB_PLUGIN_URL .'/css/tabcontent.css', array(), '1.0', 'all' );
		wp_enqueue_style( 'colorpicker-css', WPKB_PLUGIN_URL .'/css/colorpicker.css', array(), '1.0', 'all' );
		wp_enqueue_style( 'datatables-css', WPKB_PLUGIN_URL .'/js/DataTables/css/jquery.dataTables.min.css' );
		wp_enqueue_style( 'datatables-css', WPKB_PLUGIN_URL .'/js/DataTables/css/jquery.dataTables_themeroller.css' );
		wp_enqueue_style( 'kbucket-facebox-css', WPKB_PLUGIN_URL . '/js/facebox/src/facebox.css' );
		wp_enqueue_style( 'kbucket-css', WPKB_PLUGIN_URL .'/css/kbucket-style.css', array( 'kbucket-facebox-css' ) );

		wp_enqueue_script( 'jquery-validate-js', WPKB_PLUGIN_URL . '/js/jquery.validate.js' );
		wp_enqueue_script( 'tabcontent-js', WPKB_PLUGIN_URL .'/js/tabcontent.js' );
		wp_enqueue_script( 'datatables-js', WPKB_PLUGIN_URL .'/js/DataTables/js/jquery.dataTables.min.js' );
		wp_enqueue_script( 'colorpicker-js', WPKB_PLUGIN_URL . '/js/colorpicker.js' );
		wp_enqueue_script( 'admin-js', WPKB_PLUGIN_URL . '/js/admin.js' );

		wp_localize_script(
		 'admin-js',
		 'ajaxObj',
		 array(
			 'ajaxurl' => admin_url( 'admin-ajax.php' ),
			 'adminUrl' => admin_url() . 'admin.php',
			 'pluginUrl' => WPKB_PLUGIN_URL,
			 'kbucketUrl' => KBUCKET_URL,
			)
		);
	}

	/**
	 * Add Kbucket section page in dashboard
	 * Fired by wp action hook: admin_menu
	 */
	public function action_admin_menu() {
		add_menu_page( 'Page Title', 'KBucket', 'administrator', 'KBucket', array( $this, 'render_dashboard' ) );
	}

	/**
	 * Set custom template for subsections or mobile
	 * Fired by wp action hook: page_template
	 * @param $template
	 * @return string
	 */
	public function action_set_page_template( $template )
	{
		if ( $this->template !== '' ) {
			return WPKB_PATH . 'templates/' . $this->template;
		}

		return $template;
	}

	/**
	 * @param $postId
	 * @param $post
	 */
	public function action_update_kbucket_from_sticky( $postId, $post )
	{
		$kbucketId = get_post_meta( $postId, '_id_kbucket', true );

		if ( $kbucketId ) {
			$res = $this->wpdb->update(
			 $this->wpdb->prefix . 'kbucket',
			 array(
				 'title' => $post->post_title,
				 'description' => $post->post_content,
			 ),
			 array( 'id_kbucket' => $kbucketId ),
			 '%s',
			 '%s'
			);

		}
	}

	/*****************************
	 * Content Filtering functions
	 *****************************
	 */

	/**
	 * Change default page title to current Kbucket category name
	 * Fired by wp_title filter hook
	 * @return mixed
	 */
	public function filter_the_title() {

		$title = $this->category->name;

		return $title;
	}

	/**
	 * Replace post link URL with Kbuckets listings page URL with share window
	 * Fired by wp filter hook: post_link
	 * @param $url
	 * @param $post
	 * @return string
	 */
	public function filter_post_link( $url, $post )
	{
		$kbucketUrl = get_post_meta( $post->ID, '_url_kbucket', true );

		return $kbucketUrl !== '' ? WPKB_SITE_URL . '/'  . $kbucketUrl : $url;
	}

	/**
	 * Render head content
	 * Fired by wp_head filter hook
	 * @param $content string
	 */
	public function filter_wp_head( $content ) {

		// If Kbucket id is set,it's a link to share box.
		if ( $this->kbucket ) {
			$shareData = $this->get_kbucket_share_data( $this->kbucket );

			$this->metaOgUrl = WPKB_SITE_URL . '/' . $shareData['url'] . '/';

			 // Set the metatags for Facebook Open Graph and Linkedin callback request
			$this->render_meta_tags(
			 $shareData['imageUrl'],
			 $shareData['title'],
			 $shareData['description']
			);

		} elseif ( isset( $this->category->name ) ) {
			$imageUrl = ! empty( $this->category->image )
				? WPKB_PLUGIN_URL . '/uploads/categories/' . $this->category->image
				: '';

			$this->metaOgUrl = $this->category->url; ?>
			<!-- Generated by Kbucket -->
			<?php
			// Set the metatags for Facebook Open Graph and Linkedin callback request
			$this->render_meta_tags( $imageUrl, $this->category->name, $this->category->description ); ?>
			<!-- /Generated by Kbucket -->
			<?php
		} ?>

		<script type="text/javascript">
		/* <![CDATA[ */
		<?php
		if ( 1 == $this->kbPage->settings->site_search ): ?>
			var kbSearch = '',
			permalink = '<?php echo esc_js( get_permalink() ); ?>',
			searchRes = '<?php echo esc_js( $_REQUEST['srch'] ); ?>';
		<?php endif; ?>
		/* ]]> */
		</script>
		<?php
		return $content;
	}

	/**
	 * Render KBucket listings page content
	 * Fired by wp filter hook: the_content
	 */
	public function filter_the_content( $content ) {

		ob_start();

		$localize = array(
			'kbucketUrl' => WPKB_PLUGIN_URL,
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		);

		if ( $this->isMobile ) {

			if ( $this->kbucket ) {

				$this->render_kbucket_page_mobile();

				$kbContent = ob_get_contents();

				ob_end_clean();

				return $kbContent;
			}

			$this->render_head_mobile();

			$this->render_content_mobile();

			$kbContent = ob_get_contents();

			ob_end_clean();

			return $kbContent;
		}


		// If it's not a Kbucket listing page
		if ( ! $this->isKbucketPage ) {

			wp_localize_script( 'kbucket-js', 'kbObj', $localize );

			$kbContent = ob_get_contents();

			ob_end_clean();

			return $kbContent . $content;
		}

		$localize['categoryName'] = $this->category->name;
		$localize['addthisId'] = $this->addthisId;

		if ( $this->kbucket ) {
			$localize['shareId'] = $this->kbucket->id_kbucket;
		}

		wp_localize_script( 'kbucket-js', 'kbObj', $localize );

		$settings = $this->kbPage->settings;?>
		<div id="kb-wrapper"><?php

			$sortBy = ! empty( $_REQUEST['sort'] ) ? esc_html( $_REQUEST['sort'] ) : esc_html( $settings->sort_by );
			$categoryId = $this->category->id_cat;

			if ( $settings->page_title != '' ):?>
				<h2><b><?php echo esc_html( $settings->page_title ); ?></b></h2>
			<?php endif;
			$this->render_header_content( $this->category );

			if ( 1 == $settings->site_search && isset( $_REQUEST['srch'] ) ):

				$this->render_search_results( $this->category );

				$kbContent = ob_get_contents();

				ob_end_clean();

				return $kbContent . $content;

			endif;
			?>
			<a href="<?php echo KBUCKET_URL; ?>?kbt=suggest" id="kb-suggest" class="kb-suggest" title="suggest content">
				<img src="<?php echo WPKB_PLUGIN_URL; ?>/images/suggest_content.png" alt="Suggest Content">
			</a>
			<div id="kb-items">
				<a href="<?php echo WPKB_PLUGIN_URL . '/rss.php?c=' . $categoryId; ?>" id="kb-rss" target="_blank">RSS</a>
				<div class="kb-sort">
					<span style="color:<?php echo esc_attr( $this->kbPage->settings->header_color ); ?>;">Sort by:</span>
					<a href="<?php echo esc_attr( $this->get_current_url( 'author' ) ); ?>" style="color:<?php
					echo $sortBy == 'author'
						? esc_attr( $this->kbPage->settings->main_tag_color1 )
						: esc_attr( $settings->main_tag_color2 );
					?>;">Author</a>
					<a href="<?php echo esc_attr( $this->get_current_url( 'add_date' ) ); ?>" style="color:<?php
					echo $sortBy == 'add_date'
						? esc_attr( $settings->main_tag_color1 )
						: esc_attr( $settings->main_tag_color2 );
					?>;">Date</a>
					<a href="<?php echo esc_attr( $this->get_current_url( 'title' ) ); ?>" style="color:<?php
					echo $sortBy == 'title'
						? esc_attr( $settings->main_tag_color1 )
						: esc_attr( $settings->main_tag_color2 );
					?>;">Title</a>
				</div>
				<div id="kb-items-list">
					<?php
					if ( count( $this->kbPage->buckets ) > 0 ):
						$this->render_kbuckets_list( $this->kbPage->buckets );
						else : ?>
							<div class="kb-list-item"><b>No result found. Please refine your search</b></div>
						<?php endif; ?>
				</div>
				<span id="kb-pages-count"><?php
					echo 'Page ' . (int) $this->kbPage->currentPageI . ' of ' . (int) $this->kbPage->pagesCount; ?></span>
				<div id="kb-pagination">
					<?php $this->render_pagination_links(); ?>
				</div>
			</div>
			<div id="kb-tags-wrap">
				<?php

				// Render category tags
				$categoryTags = $this->get_category_tags();
				if ( ! empty( $categoryTags ) ) {
					$this->render_tags_cloud(
					 $categoryTags,
					 'm',
					 'main',
					 array( 'value' => $this->kbPage->activeTagName, 'dbKey' => 'name', 'title' => 'name' ),
					 array( 'mtag' => 'name' )
					);
				}

				// Render related tags
				if ( ! empty( $this->kbPage->relatedTags ) ) { ?>
					<h3>Related Tags</h3>
					<?php
					$this->render_tags_cloud(
					 $this->kbPage->relatedTags,
					 'r',
					 'related',
					 array( 'value' => $this->kbPage->relatedTagName, 'dbKey' => 'name', 'title' => 'name' ),
					 array( 'mtag' => array( 'raw' => $this->kbPage->activeTagName ), 'rtag' => 'name' )
					);
				}

				// Render Author tags
				if ( $this->kbPage->settings->author_tag_display && isset( $this->kbPage->authorTagName ) ) { ?>
					<h3>Author Tags</h3>
					<?php
					$authorTags = $this->get_author_tags();

					$this->render_tags_cloud(
					 $authorTags,
					 'a',
					 'author',
					 array( 'value' => $this->kbPage->authorTagName, 'dbKey' => 'author', 'title' => 'author' ),
					 array( 'atag' => 'author' )
					);
				} ?>
			</div>
		</div>
		<?php $this->render_category_disqus(); ?>
		<noscript>
			Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a>
		</noscript>
		<?php
		$kbContent = ob_get_contents();
		ob_end_clean();

		return $kbContent . $content;
	}


	/**
	 * Render Footer content
	 * Fired by wp_footer filter hook
	 * @param $content
	 * @return string
	 */
	public function filter_wp_footer( $content ) {
		?>
		<div class="addthis_toolbox"></div>
		<?php
		if ( $this->isMobile ) {
			return $content;
		}
		
		$this->render_addthis_config();

		return $content;
	}

	/*****************************
	 * Content rendering functions
	 *****************************
	 */

	/**
	 * Render Head content for mobile version
	 * Fired by wp_head filter hook
	 */
	private function render_head_mobile()
	{
		$page = $this->kbPage;
		?>
		<div id="kb-tags" class="sidebar" data-role="panel" data-position="left" data-display="overlay" data-dismissible="false">
			<div class="ui-corner-all custom-corners">
				<a href="#kb-list" data-rel="close" class="ui-btn">Close</a>
				<div class="ui-bar ui-bar-a">
					<h3>Tags</h3>
				</div>
				<div class="ui-body ui-body-a">
					<?php
					$categoryTags = $this->get_category_tags();
					if ( ! empty( $categoryTags ) ) {
						$this->render_tags_cloud(
						 $categoryTags,
						 'm',
						 'main',
						 array( 'value' => $this->kbPage->activeTagName, 'dbKey' => 'name', 'title' => 'name' ),
						 array( 'mtag' => 'name' )
						);
					}
					?>
				</div>
				<a href="#kb-list" data-rel="close" class="ui-btn">Close</a>
			</div>
		</div>
		<div data-role="header" class="ui-bar ui-bar-a ui-corner-all">
			<div id="kb-main-menu" data-role="navbar">
				<ul>
					<li><a href="<?php echo WPKB_SITE_URL; ?>">Home</a></li>
				</ul>
			</div>

			<h1><?php echo esc_html( $this->category->name ); ?></h1>

			<div data-role="tabs" id="tabs">
				<div data-role="navbar">
					<ul>
						<?php
						$subcategories = array();
						foreach ( $page->menu['categories'] as $c ): ?>
						<li><?php
						if ( $this->category->id_cat == $c->id_cat
							|| ( isset( $this->category->parent_cat ) && $this->category->parent_cat == $c->id_cat ) ):?>
							<a href="#menu-subcategories-<?php
								echo esc_attr( $c->id_cat ); 
							?>" class="ui-btn-active ui-state-persist" data-ajax="false"><?php
								echo esc_html( $c->name ); ?></a>
						<?php else: ?>
							<a href="#menu-subcategories-<?php echo esc_attr( $c->id_cat ); ?>" data-ajax="false"><?php
							echo esc_html( $c->name ); ?></a><?php
						endif;
							$subcategories[$c->id_cat] = $c->subcategories;
						?></li>
						<?php endforeach; ?>
					</ul>
				</div>

				<?php
				foreach ( $subcategories as $categoryId => $subcategories ): ?>
				<div id="menu-subcategories-<?php echo esc_attr( $categoryId ); ?>">
					<ul data-role="listview" data-inset="true">
					<?php
					foreach ( $subcategories as $subcategoryId ): ?>
						<li><?php
							$subcategory = $page->menu['subcategories'][$subcategoryId];
							echo $this->category->id_cat == $subcategory->id_cat
									? esc_html( $subcategory->name )
									: '<a href="' . esc_attr( $subcategory->url )
									  .'"  rel="external" data-ajax="false">'. esc_html( $subcategory->name ) . '</a>';
						?></li>
						<?php endforeach; ?>
					</ul>
				</div>
				<?php endforeach; ?>
			</div>
			<div data-role="navbar">
				<ul>
					<li><a href="#kb-tags" data-ajax="false">Tags Cloud</a></li>
				</ul>
			</div>
		</div>
		<?php
	}

	private function render_kbucket_page_mobile()
	{
		// Get sharing data
		$shareData = $this->get_kbucket_share_data( $this->kbucket );

		$url = WPKB_SITE_URL . '/' . $shareData['url'];
		?>
		<div role="main" class="ui-content">
			<div class="ui-grid-solo">
				<div style="text-align: center;!important">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- EON LMB -->
					<ins class="adsbygoogle" style="display:inline-block;width:320px;height:100px" data-ad-client="<?php
						echo esc_attr( $this->googleadsClient ); ?>" data-ad-slot="<?php
						echo esc_attr( $this->googleadsSlotMobile ); ?>"></ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
				<?php if ( ! empty( $shareData['imageUrl'] ) ): ?>
					<img src="<?php
					echo esc_attr( $shareData['imageUrl'] );
					?>" class="share-image" style="max-width:300px;max-height:300px"/>
				<?php endif; ?>
				<h2><a href="<?php echo esc_attr( $this->kbucket->link ); ?>" rel="external" data-ajax="false"><?php
						echo esc_html( $shareData['title'] ); ?></a></h2>
				<span class="kb-item-date" style="color:<?php echo esc_attr( $this->kbPage->settings->author_color );
				?> !important;"><?php echo esc_html(  $shareData['kbucket']->add_date ); ?> by </span>
				<span style="color:<?php echo esc_attr( $this->kbPage->settings->author_color ); ?> !important;"><?php
					echo esc_html( $shareData['kbucket']->author ); ?></span>
				<p>
					<span style="color:<?php
					echo esc_attr( $this->kbPage->settings->content_color );?> !important;"><?php
						echo wp_kses(
						 $this->kbucket->description,
						 array(
							'a' => array(
								'href' => array(),
								'title' => array()
							),
							'br' => array(),
						 )
						); ?></span>
				</p>
			</div>
		</div>
		<?php
		$this->render_footer_links();
	}
	
	/**
	 * Render page content for mobile version
	 * Fired by the_content filter hook
	 * @return string
	 */
	public function render_content_mobile()
	{
		$murl = $this->get_current_url( 'mtag' ); ?>

		<div role="main" id="kb-items-list" class="ui-content">
			<ul data-role="listview" data-inset="true" data-theme="a" data-divider-theme="a" class="ui-corner-all">
				<?php foreach ( $this->kbPage->buckets as $m ): ?>
					<li class="ui-corner-all kb-list-item">
						<p><?php echo esc_html( $m->add_date ); ?></p>
						<h2><a href="<?php echo esc_attr( $m->link ); ?>"><?php echo esc_html( $m->title ); ?></a></h2>
						<p><strong><?php echo esc_html( $m->author ); ?></strong></p>
						<p class="kb-wrap"><?php
							echo wp_kses(
							 $this->kbucket->description,
							 array(
								'a' => array(
									'href' => array(),
									'title' => array()
								),
								'br' => array(),
							 )
							) ?></p>
						<div data-role="controlgroup" data-type="horizontal" class="ui-mini kb-tag-links">
							<?php $this->render_kbucket_tags( $murl, $m->tags, ' ui-btn' ); ?>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
			<?php $this->render_category_disqus();?>
		</div><?php
		$this->render_footer_mobile();
		?>
		<script type="text/javascript">
			jQuery(document).ready(function ($kbj) {
				$kbj('#kb-main-menu a').attr('data-ajax', 'false');
				if (typeof DISQUS !== 'undefined') {
					DISQUS.reset({
						reload: true,
						config: function () {  
						this.page.identifier = disqus_identifier;
						this.page.url = disqus_url;
						}
					});
				}
			});
		</script><?php
	}

	/**
	 * Render Footer content for mobile version
	 * Fired by wp_footer filter hook
	 * @return string
	 */
	public function render_footer_mobile()
	{
		?>
		<div id="kb-footer" data-role="footer" style="overflow:hidden;">
			<h4 style="text-align:center;"><?php
				echo 'Page: ' . (int) $this->kbPage->currentPageI . ' of ' . (int) $this->kbPage->pagesCount; ?></h4>
			<div data-role="navbar">
				<ul>
					<?php $this->render_pagination_links(); ?>
				</ul>
			</div>
		</div>
		<?php
		$this->render_footer_links();
	}

	public function render_footer_links()
	{ ?>
		<div class="ui-bar-a">
			<p>
				<a href="<?php echo WPKB_SITE_URL; ?>" rel="external" data-ajax="false">View Full Site</a> |
				<a href="<?php echo KBUCKET_URL; ?>" rel="external" data-ajax="false">Kbuckets</a>
			</p>
			<p>
				<a href="<?php echo KBUCKET_URL; ?>/?mobile=n" rel="external" data-ajax="false">Switch to full version</a>
			</p>
		</div>
		<?php $this->render_addthis_config();
	}
	
	private function render_category_disqus()
	{
		// Ignore Disqus if there is no shortname set (comment system plugin not installed?)
		if ( $this->disqusConfig['shortname'] == '' ) {
			return false;
		}?>
		<div id="disqus_thread"></div>
		<?php
		$this->render_disqus_config(
		 $this->disqusConfig['shortname'],
		 $this->category->name,
		 $this->category->url,
		 $this->category->id_cat
		);
	}

	/**
	 * Render Meta tags for share scripts
	 * @param $imageUrl
	 * @param $title
	 * @param $description
	 */
	public function render_meta_tags( $imageUrl, $title, $description )
	{
		$imageUrl = esc_url( $imageUrl );
		$title = esc_attr( $title );
		$url = esc_url( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
		$description = esc_attr( $description );
		?>
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="0" />
		<?php /*<meta property="og:site_name" content="<?php echo esc_attr( $site ); ?>" /> */?>
		<meta property="og:url" content="<?php echo $url; ?>">
		<meta property="og:type" content="website" />
		<meta name="twitter:card" content="summary" />
		<meta property="og:title" content="<?php echo $title; ?>"/>
		<meta name="twitter:title" content="<?php echo $title; ?>" />
		<meta name="bitly-verification" content="2809eb754836"/>
		<?php if ( ! empty( $imageUrl ) ): ?>
		<link rel="image_src" href="<?php echo $imageUrl; ?>" />
		<meta property="og:image" content="<?php echo $imageUrl; ?>" />
		<meta name="twitter:image:src" content="<?php echo $imageUrl; ?>" />
		<?php endif;
		if ( ! empty( $description ) ): ?>
			<meta name="description" content="<?php echo $description; ?>" />
			<meta property="og:description" content="<?php echo $description; ?>" />
			<meta name="twitter:description" content="<?php echo $description; ?>" /><?php
		endif;
	}

	/**
	 * Render Addthis Share toolbox
	 * @param array $data
	 */
	private function render_share_toolbox( $data = array() )
	{
		$lastChar = substr( $data['url'], -1 );
		// Stupid hack to get deal with Facebook share (URL needs to be ended with slash to prevent 301 redirect)
		if ( $lastChar !== '/' ) {
			$data['url'] .= '/';
		}
		?>
		<div class="addthis_toolbox addthis_default_style addthis_32x32_style"<?php
		if ( ! empty( $data['url'] ) ) {
			echo ' data-url="' . esc_attr( $data['url'] ) . '" addthis:url="' . esc_attr( $data['url'] ) . '"';
		}
		if ( ! empty( $data['title'] ) ) {
			echo ' data-title=" ' . esc_attr( $data['title'] ) . '" addthis:title=" ' . esc_attr( $data['title'] ) . '"';
		}
		if ( ! empty( $data['image'] ) ) {
			echo ' addthis:image="' . esc_attr( $data['image'] ) . '"';
		}
		if ( ! empty( $data['description'] ) ) {
			echo ' addthis:description="' . esc_attr( $data['description'] ) . '"';
		}?>>
			<?php
			foreach ( $this->addthisShareButtons as $service => $values ): ?>
			<a class="addthis_button_<?php echo esc_attr( $service ); ?>"<?php
			if ( is_array( $values ) ) {
				foreach ( $values as $key => $val ) {
					echo ' ' . esc_attr( $key ). '="' . esc_attr( $val ) . '"';
				}
			}
			?>></a>
			<?php endforeach ; ?>
		</div>
	<?php
	}

	/**
	 * Render Kbuckets list
	 * @param $kbuckets
	 * @param bool $share
	 */
	private function render_kbuckets_list( $kbuckets, $share = true )
	{
		foreach ( $kbuckets as $i => $m ):
			$description = wp_kses(
				$m->description,
				array(
					'a' => array(
						'href' => array(),
						'title' => array()
					),
					'br' => array(),
				)
			); ?>
			<div id="kb-item-<?php echo esc_attr( $m->kbucketId );?>" class="kb-item">
				<h3>
					<?php if ( $share ): ?>
					<a style="color:<?php echo esc_attr( $this->kbPage->settings->tag_color );
					?>;" id="kb-share-item-<?php echo esc_attr( $m->kbucketId );?>" class="kb-share-item" title="<?php
					echo esc_attr( $m->title ); ?>">
						<img src="<?php echo WPKB_PLUGIN_URL . '/images/kshare.jpeg'; ?>" alt="Share Button"/>
					</a>
					<?php endif; ?>
					<a href="<?php echo esc_attr( $m->link ); ?>" class="kb-link" target="_blank" style="color:<?php
						echo esc_attr( $this->kbPage->settings->header_color );
					?>;"><?php echo esc_html( $m->title ); ?></a>
				</h3>
				<span class="kb-item-author" style="color:<?php
					echo esc_attr( $this->kbPage->settings->author_color );
				?>;">Author: <span class="kb-item-author-name"><?php echo esc_html( $m->author ); ?></span>
				</span>
				<span class="kb-item-date" style="color:<?php
					echo esc_attr( $this->kbPage->settings->author_color );
				?> !important;"><?php echo esc_html( $m->add_date ); ?></span>
				<p style="color:<?php echo esc_attr( $this->kbPage->settings->content_color ); ?>;"><?php
					echo substr( $description, 0, 100 );
				if ( strlen( $description ) > 100 ):
					?><span style="color:<?php
					echo esc_attr( $this->kbPage->settings->content_color ); ?>;display:none" id="kb-item-text-<?php
					echo (int) $i; ?>"><?php
				echo substr( $description, 100, strlen( $description ) );
				?></span> <span style="color:<?php
					echo esc_attr( $this->kbPage->settings->content_color ); ?>;cursor:pointer;" id="kb-toggle-<?php
					echo (int) $i; ?>" class="kb-toggle">[+]</span>
					<?php
					else :
						echo $description;
					endif; ?>
				</p>
				<p class="blue" style="margin-bottom:2px;">
					<span style="color:<?php echo esc_attr( $this->kbPage->settings->tag_color ); ?>;">Tags:</span>
					<?php $this->render_kbucket_tags( $this->kbucketCurrentUrl, $m->tags );?>
				</p>
			</div>
		<?php endforeach;
	}

	/**
	 * Render header content
	 */
	public function render_header_content() {

		$searchtxt = isset( $_REQUEST['srch'] ) ? $_REQUEST['srch'] : ''; ?>
		<div id="kb-header">
			<div class="kb-right-cont">
				<div id="kb-search">
					<?php
					if ( 1 == $this->kbPage->settings->site_search ): ?>
						<form method="POST">
							<input type="text" name="srch" value="<?php
							echo esc_attr( $searchtxt ); ?>" id="kb-search-input"/>
							<input type="submit" value="Search"/>
						</form>
					<?php endif;?>
				</div>
			</div>
			<div id="kb-menu">
				<ul>
					<?php
					$subcategories = array();
					foreach ( $this->kbPage->menu['categories'] as $c ) {
						$class = $this->category->id_cat == $c->id_cat
								|| ( isset( $this->category->parent_cat ) && $this->category->parent_cat == $c->id_cat )
						? ' class="kb-menu-active"'
						: ''; ?>
					<li><a href="<?php echo esc_attr( $c->url ) . '"' . $class; ?>><?php echo esc_html( $c->name ); ?></a></li>
					<?php
					if ( $this->category->id_cat == $c->id_cat ) {
						$subcategories = $c->subcategories;
					}
					} ?>
				</ul>
			</div>
			<div id="kb-submenu">
				<ul>
					<?php
					if ( isset( $this->category->parent_cat ) ) {
						$subcategories = $this->kbPage->menu['categories'][ $this->category->parent_cat ]->subcategories;
					}
					foreach ( $subcategories as $subcategoryId ) {
						$subcategory = $this->kbPage->menu['subcategories'][ $subcategoryId ];
						$class = $this->category->id_cat == $subcategoryId ? ' class="kb-menu-active"' : '';
					?><li><a href="<?php echo esc_attr( $subcategory->url ); ?>"<?php echo $class; ?>><?php
						echo esc_html ( $subcategory->name ); ?></a></li>
					<?php
					}?>
					<li id="kb-search-button"><a href="#">Search<a/></li>
				</ul>
			</div>
		</div><?php
	}

	/**
	 * Render Javascript section with Addthis configuration variables
	 */
	private function render_addthis_config()
	{ ?>
		<script type="text/javascript">
			var addthis_config = {
					pubid: "<?php echo esc_js( $this->addthisId ); ?>",
					"data_track_addressbar" : true,
					"ui_508_compliant" : true
				},
				addthis_share = {
					url_transforms : {
						shorten: {
							twitter: 'bitly'
						}
						<?php
						if ( ! empty( $_GET ) ): ?>,
						add: {
							<?php
							foreach ( $_GET as $key => $val ) {
								echo esc_js( $key ) . ":'" . esc_js( $val ) . "',";
							} ?>
						}
						<?php endif; ?>
					},
					shorteners : {
						bitly : {}
					}
				};
		</script>
		<?php
	}

	/**
	 * Customize search query join part to include kbuckets
	 * @param $query - passed by reference,therefore modified directly
	 */
	public function kb_search_join( $join )
	{
		if( is_search() ) {
			
			global $wpdb;
			
			$join .= 
		}
		
		echo $join;
		return $join;
	}
	
	
	/**
	 * Customize search query criteria
	 * @param $query - passed by reference,therefore modified directly
	 */
	public function kb_search_query($where)
	{
		if( is_search() ) {
			global $wpdb;
			$query = get_search_query();
			$query = $wpdb->esc_like( $query );
			
			// include postmeta in search
			//$where .=" OR {$wpdb->posts}.ID IN (SELECT {$wpdb->postmeta}.post_id FROM {$wpdb->posts}, {$wpdb->postmeta} WHERE {$wpdb->postmeta}.meta_key = 'objectif' AND {$wpdb->postmeta}.meta_value LIKE '%$query%' AND {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id)";
			// include taxonomy in search
			//$where .=" OR {$wpdb->posts}.ID IN (SELECT {$wpdb->posts}.ID FROM {$wpdb->posts},{$wpdb->term_relationships},{$wpdb->terms} WHERE {$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id AND {$wpdb->term_relationships}.term_taxonomy_id = {$wpdb->terms}.term_id AND {$wpdb->terms}.name LIKE '%$query%')";
			
			if(WP_DEBUG) var_dump($where);
    }
		return $where;
	}
	
	/**
	 * Render search content
	 */
	private function render_search_results() {
		$srch = trim( substr( $_REQUEST['srch'],0, 100 ) );

		if ( $srch == '' ) {
			return false;
		}

		$srch = urldecode( $srch );
		$highlight = '<span class="kb-hlt">' . $srch . '</span>';

		if ( strlen( $srch ) < 2 ):?>
			<h1 class="page-title">No results found for your search</h1>
			<?php return false; endif; ?>
		<h1 class="page-title">Searching in Kbucket, Cabinet, Drawer, Link for <b><?php echo esc_html( $srch ); ?></b></h1>
		<?php
		$sql = 'SELECT id_cat,name
				FROM `' . $this->wpdb->prefix . "kb_category`
				WHERE parent_cat=0 AND name like '%%%s%%'";

		$query = $this->wpdb->prepare( $sql, $srch );
		$data = $this->wpdb->get_results( $query );

		foreach ( $data as $m ): ?>
			<div class="kb-item">
				<h3>
					<span style="background-color: #217BBA;color: #FFFFFF;padding: 1px 3px;">KBUCKET</span>
					<a href="<?php echo KBUCKET_URL . '/kc/' . $m->id_cat; ?>" class="highlight"><?php echo str_ireplace( $srch, $highlight, $m->name ); ?></a>
				</h3>
			</div>
		<?php
		endforeach;

		$sql = 'SELECT c.`id_cat` AS parid,
						b.`id_cat` AS subid,
						c.`name` AS parcat,
						b.`name` AS subcat
				FROM `' . $this->wpdb->prefix . 'kb_category` b
				INNER JOIN `' . $this->wpdb->prefix . "kb_category` c
					ON c.`id_cat` = b.`parent_cat`
				WHERE b.`parent_cat`<>0 AND b.`name` LIKE '%%%s%%'";

		$query = $this->wpdb->prepare( $sql, $srch );
		$data = $this->wpdb->get_results( $query );

		foreach ( $data as $m ): ?>
			<div class="kb-item">
				<h3>
					<span style="background-color: #217BBA;color: #FFFFFF;padding: 1px 3px;">KBUCKET</span>
					<a href="<?php echo KBUCKET_URL . '/kc/' .$m->parid; ?>"><?php
						echo str_ireplace( $srch, $highlight, $m->parcat ); ?></a>
					/
					<a href="<?php echo KBUCKET_URL . '/kc/'  . $m->subid?>"><?php
						echo str_ireplace( $srch, $highlight, $m->subcat ); ?></a>
				</h3>
			</div>
		<?php
		endforeach;

		$sql = 'SELECT a.`id_kbucket`
				FROM `' . $this->wpdb->prefix . 'kbucket` a
				INNER JOIN `' . $this->wpdb->prefix . 'kb_tags` b ON a.`id_kbucket` = b.`id_kbucket`
				INNER JOIN `' . $this->wpdb->prefix . "kb_tag_details` c ON b.`id_tag` = c.`id_tag`
				WHERE
				(
					a.`title` LIKE '%%%s%%'
					OR a.`description` LIKE '%%%s%%'
					OR a.`author`=%s
					OR c.`name`=%s
				)
				GROUP BY a.`id_kbucket`
				LIMIT 100";

		$query = $this->wpdb->prepare( $sql, array( $srch, $srch, $srch, $srch ) );
		$res = $this->wpdb->get_results( $query );

		$id_kbucket = array();

		foreach ( $res as $row ) {
			$id_kbucket[] = $row->id_kbucket;
		}
		$id_kbucket = "'" . implode( "','", esc_sql( $id_kbucket ) ) . "'";

		$sql = 'SELECT a.`id_kbucket`,GROUP_CONCAT(`name`) AS tags
				FROM `' . $this->wpdb->prefix . 'kbucket` a
				INNER JOIN `' . $this->wpdb->prefix . 'kb_tags` b
					ON b.`id_kbucket` = a.`id_kbucket`
				INNER JOIN `' . $this->wpdb->prefix . "kb_tag_details` c
					ON c.`id_tag` = b.`id_tag`
				WHERE a.`id_kbucket` IN ( $id_kbucket)
				GROUP BY a.`id_kbucket`
				LIMIT 100";

		$tag = array();

		$res = $this->wpdb->get_results( $sql );

		foreach ( $res as $r ) {
			$tag[ $r->id_kbucket ] = $r->tags;
		}

		$sql = 'SELECT a.`id_kbucket`,
						a.`id_cat`,
						a.`title`,
						a.`description`,
						a.`link`,
						a.`author`,
						STR_TO_DATE(a.`pub_date`, "%a, %d %b %Y" ) AS postedDate,
						c.`id_cat` AS parid,
						b.`id_cat` AS subid,
						c.`name` AS parcat,
						b.`name` AS subcat
				FROM `' . $this->wpdb->prefix . 'kbucket` a
				INNER JOIN `' . $this->wpdb->prefix . 'kb_category` b ON a.`id_cat` = b.`id_cat`
				INNER JOIN `' . $this->wpdb->prefix . "kb_category` c ON c.`id_cat` = b.`parent_cat`
				WHERE a.`id_kbucket` IN ( $id_kbucket)
				LIMIT 100";

		$data = $this->wpdb->get_results( $sql );

		$i = 0;
		foreach ( $data as $m ) {
			$i ++;
			$description = wp_kses(
				$m->description,
				array(
					'a' => array(
						'href' => array(),
						'title' => array()
					),
					'br' => array(),
				)
			);?>
			<div id="kb-item-<?php echo esc_attr( $m->id_kbucket );?>" class="kb-item">
				<h3>
					<a style="color:<?php echo esc_attr( $this->kbPage->settings->tag_color ); ?>;" id="kb-share-item-<?php
					echo esc_attr( $m->id_kbucket ); ?>" class="kb-share-item" title="<?php echo esc_html( $m->title ); ?>">
						<img src="<?php echo WPKB_PLUGIN_URL . '/images/kshare.jpeg'; ?>" alt="Share Button"/>
					</a>
					<span style="background-color: #217BBA;color: #FFFFFF;padding: 1px 3px; line-height:35px;">KBUCKET</span>
					<a href="<?php
						echo KBUCKET_URL . '/kc/' . $m->parid;
					?>" style="color:<?php echo esc_attr( $this->kbPage->settings->header_color ); ?>;"><?php
						echo str_ireplace( $srch, $highlight, $m->parcat );
					?></a>
					/
					<a href="<?php
						echo KBUCKET_URL . '/kc/' . $m->subid;
					?>" style="color:<?php echo esc_attr( $this->kbPage->settings->header_color ); ?>;"><?php
						echo str_ireplace( $srch, $highlight, $m->subcat );
					?></a>
					/
					<a href="<?php echo esc_attr( $m->link ); ?>" target="_blank" style="color:<?php
						echo esc_attr( $this->kbPage->settings->header_color );
					?>;"><b><?php echo str_ireplace( $srch, $highlight, $m->title ); ?></b></a>
				</h3>
				<span style=" margin:0px; color:<?php
					echo esc_attr( $this->kbPage->settings->author_color );
				?>;">Author: <?php echo str_ireplace( $srch, $highlight, $m->author ); ?></span>
				<span class="kb-item-date" style="color:<?php
					echo esc_attr( $this->kbPage->settings->author_color ); ?>;"><?php echo esc_html( $m->postedDate ); ?></span>
				<p>
					<span style="color:<?php
						echo esc_attr( $this->kbPage->settings->content_color );
						?>;"><?php
							echo str_ireplace( $srch, $highlight, substr( $description, 0, 100 ) );
						?></span>
						<?php
						if ( strlen( $description ) > 100 ): ?>
							<span style="color:<?php
								echo esc_attr( $this->kbPage->settings->content_color );
							?>;display:none;" id="kb-item-text-<?php echo (int) $i; ?>">
						<?php
						echo 
						str_ireplace(
						 $srch,
						 $highlight,
						 ( substr($description, 100, strlen( $description ) ) )
						); ?>
					</span>
					<span style="color:<?php
						echo esc_attr( $this->kbPage->settings->content_color );
					?>;cursor:pointer;" id="kb-toggle-<?php echo (int) $i; ?>" class="kb-toggle">[+]</span>
					<?php endif; ?>
				</p>
				<p class="blue" style=" margin-bottom:2px;">
						<span style="color:<?php echo esc_attr( $this->kbPage->settings->tag_color ); ?>;">Tags: <?php
							echo str_ireplace( $srch, $highlight, $tag[ $m->id_kbucket ] ); ?></span>
				</p>
			</div>
		<?php
		}

		$res = $this->create_search_condition( $srch, array( 'post_title', 'post_content' ), array( '=', 'LIKE' ) );
		$conditions = $res['conditions'];
		$values = $res['values'];

		$sql = 'SELECT a.`ID`,`user_nicename`,DATE(`post_date`) AS dt,`post_content`,`post_title`
				FROM `' . $this->wpdb->prefix . 'posts` a
				INNER JOIN `' . $this->wpdb->prefix . "users` b ON a.`post_author` = b.`ID`
				WHERE ( $conditions)
					AND `post_type` IN ( 'post','page' )
					AND `post_status` = 'publish'
				LIMIT 100";

		$query = $this->wpdb->prepare( $sql, $values );
		$data = $this->wpdb->get_results( $query );

		foreach ( $data as $m ): ?>
			<div class="kb-item">
				<h3>
					<span style="background-color: #217BBA;color: #FFFFFF;padding: 1px 3px;">LINK</span>
					<a href="<?php echo esc_attr( get_permalink( $m->ID ) ); ?>"
					   target="_blank"><?php echo str_ireplace( $srch, $highlight, $m->post_title ); ?></a>
				</h3>
				<p>Author: <?php echo str_ireplace( $srch, $highlight, $m->user_nicename ); ?></p>
				<p>Date:<?php echo esc_html( $m->dt ); ?></p>
					<span><?php
						echo str_ireplace( $srch, $highlight, substr( strip_tags( $m->post_content ), 0, 300 ) );
					?></span>
			</div>
		<?php
		endforeach;

		return true;
	}

	/**
	 * Render pagination
	 */
	private function render_pagination_links()
	{
		$start = $this->kbPage->currentPageI - 3 > 0 ? $this->kbPage->currentPageI - 3 : 1;
		$end = $this->kbPage->currentPageI + 3 < $this->kbPage->pagesCount
			? $this->kbPage->currentPageI + 3
			: $this->kbPage->pagesCount;
		$purl = $this->get_current_url( 'pagin' );

		for ( $i = $start; $i <= $end; $i ++ ) {
			$mobileAttr = '';
			if ( $this->isMobile ) {
				echo '<li>';
				$mobileAttr = ' rel="external" data-ajax="false"';
			}
			echo $this->kbPage->currentPageI == $i
				? '<a href="#ratop"' . $mobileAttr . ' class="'
				  . ( $this->isMobile ? 'ui-btn-active' : 'selected' )
				  . '">' . $i . '</a>'
				: '<a href="' . $purl . '&amp;pagin=' . $i . '"' . $mobileAttr . '> '. $i . '</a>';
			if ( $this->isMobile ) {
				echo '</li>';
			}
		}
	}

	/**
	 * Render tags cloud
	 * @param $tagsArr
	 * @param $sfont
	 * @param $stag
	 * @param $activeTag
	 * @param $linkParams
	 */
	private function render_tags_cloud( $tagsArr, $sfont, $stag, $activeTag, $linkParams )
	{
		if ( empty( $tagsArr ) ) {
			return;
		} ?>
		<div class="kb-tags" >
			<?php
			foreach ( $tagsArr as $i => $tag ):

				if ( 'related' == $stag && $tag->name == $this->kbPage->activeTagName ) {
					continue;
				}

				$fontSize = $this->kbPage->settings->{$sfont .'4'};
				$tagsCount = (int) $tag->mcnt;
				if ( $tagsCount <= 3 ) {
					$fontSize = $this->kbPage->settings->{$sfont . '1'};
				} else if ( $tagsCount > 3 and $tagsCount <= 7 ) {
					$fontSize = $this->kbPage->settings->{$sfont . '2'};
				} else if ( $tagsCount > 7 and $tagsCount <= 15 ) {
					$fontSize = $this->kbPage->settings->{$sfont . '3'};
				}

				$linkStyle = $activeTag['value'] == $tag->$activeTag['dbKey']
					? 'color:#fff;font-size:' . $fontSize . 'px;background-color:'
						.( ( $i % 2 ) == 1
							? $this->kbPage->settings->{$stag . '_tag_color1'}
							: $this->kbPage->settings->{$stag . '_tag_color1'}
						)
					: 'font-size:' . $fontSize . 'px;color:'
						. ( ( $i % 2 ) == 1
							? $this->kbPage->settings->{$stag . '_tag_color1'}
							: $this->kbPage->settings->{$stag . '_tag_color2'}
						);

				$linkUrl = $this->category->url . '?';

				$params = array();
				foreach ( $linkParams as $param => $val ) {
					$val = is_array( $val ) ? $val['raw'] : urlencode( $tag->{$val} );
					$params[] = "$param=" . urlencode( $val );
				}
				$linkUrl .= implode( '&amp;', $params );
				?>
				<a href="<?php echo esc_attr( $linkUrl ); ?>" style="<?php echo esc_attr( $linkStyle ); ?>"<?php
				if ( $this->isMobile ) {
					echo ' class="ui-btn" data-ajax="false"';
				}
				?>><?php echo esc_html( $tag->$activeTag['dbKey'] ); ?></a>
			<?php
			endforeach;?>
		</div>
	<?php
	}

	/**
	 * Render tag links for Kbucket
	 * @param $baseUrl
	 * @param $tagsStr
	 * @param string $class
	 */
	public function render_kbucket_tags( $baseUrl, $tagsStr, $class = '' )
	{
		$tagsArr = explode( '~', $tagsStr );
		$tagsStr = '';
		$attr = $this->isMobile ? ' data-ajax="false"' : '';
		foreach ( $tagsArr as $tag ) {
			$tag = explode( '|', $tag );
			if ( ! empty( $tag[1] ) ) {
				$tagsStr .= '<a class="kb-tag-link' . $class . ' style="color:'
						. esc_attr( $this->kbPage->settings->tag_color ) . ' !important;" href="'
						. $this->get_current_url( 'mtag', urlencode( $tag[1] ) ) . '"' . $attr . '>'
						. esc_html( $tag[1] ) . '</a>';

				if ( ! $this->isMobile ) {
					$tagsStr .= ',';
				}

				$tagsStr .= ' ';
			}
		}
		$tagsStr = trim( $tagsStr );
		if ( $tagsStr !== '' ) {
			echo substr( $tagsStr, 0, -1 );
		}
	}

	/**
	 * Render Categories dropdown
	 */
	public function render_categories_dropdown()
	{
		$categories = get_categories(); ?>
		<select name="sid_cat" id="sid_cat">
			<?php foreach ( $categories as $cat ): ?>
			<option style="padding-left:20px;" value="<?php
			echo esc_attr( $cat->cat_ID ); 
			?>"><?php echo esc_html( $cat->name ); ?></option>
			<?php endforeach; ?>
		</select><?php
	}

	/**
	 * Render suggest window content
	 * Fired by custom shortcode hook: suggest-page
	 */
	public function shortcode_render_suggest_page() {
		$page = $this->get_page_data();
		$this->render_header_content( $page );

		$err = '';
		if ( isset( $_POST['ssugg'] ) ) {

			$requredParams = array(
				'stitle',
				'sdesc',
				'stags',
				'surl',
				'sauthor',
			);

			foreach ( $requredParams as $param ) {
				if ( empty( $_POST[ $param ] ) ) {
					//$err = 'Failed, Please fill all required fields...';
					break;
				}
			}

			if ( $err = '' ) {

				$sql = $this->wpdb->prepare(
				 'INSERT IGNORE INTO `' . $this->wpdb->prefix . "kb_suggest`
				  SET `id_sug`='',
					`id_cat`=%s,
					`tittle`=%s,
					`description`=%s,
					`tags`=%s,
					`add_date`=CURRENT_DATE,
					`author`=%s,
					`link`=%s,
					`twitter`=%s,
					`facebook`=%s,
					`status`='0'",
				 array(
					$_POST['sid_cat'],
					$_POST['stitle'],
					$_POST['sdesc'],
					$_POST['stags'],
					$_POST['sauthor'],
					$_POST['surl'],
					$_POST['stwitter'],
					$_POST['sfacebook'],
				 )
				);

				$err = $this->wpdb->query( $sql )
					? 'Suggest link has been added successfully...'
					: 'Inserted Failed...';
			}
		} ?>
		<div class="container">
			<div class="con_suggest">
				<h1>Suggested page URL's </h1>
				<?php if ( $err != '' ): ?>
					<p><label>&nbsp;</label><span style="font-weight:bold; color:red;"><?php
							echo $err;
					?></span></p>
				<?php endif; ?>
				<form action="" method="post">
					<p>
						<label>Category</label><span><?php $this->render_categories_dropdown(); ?></span>
					</p>
					<p>
						<label for="stitle">Page Tittle* :</label>
						<span><input type="text" name="stitle" id="stitle" value="<?php
							echo @$_REQUEST['stitle']; ?>"/></span>
					</p>
					<p>
						<label for="sdesc">Description* : </label>
						<span><input type="text" name="sdesc" id="sdesc" value="<?php
							echo @$_REQUEST['sdesc']; ?>"/></span>
					</p>
					<p>
						<label for="stags">Tags* : </label>
						<span><input type="text" name="stags" id="stags" value="<?php
							echo @$_REQUEST['stags']; ?>"/></span>
					</p>
					<p>
						<label for="surl">Page URL* :</label>
						<span><input type="text" name="surl" id="surl" value="<?php
							echo @$_REQUEST['surl']; ?>"/></span>
					</p>
					<p>
						<label for="sauthor">Author* :</label>
						<span><input type="text" name="sauthor" id="sauthor" value="<?php
							echo @$_REQUEST['sauthor']; ?>"/></span>
					</p>
					<p>
						<label for="stwitter">Twitter Information page of author:</label>
						<span><input type="text" name="stwitter" id="stwitter" value="<?php
							echo @$_REQUEST['stwitter']; ?>"/></span>
					</p>
					<p>
						<label for="sfacebook">Facebook information page of author:</label>
						<span><input type="text" name="sfacebook" id="sfacebook" value="<?php
							echo @$_REQUEST['sfacebook']; ?>"/></span>
					</p>
					<p>
						<label for="ssugg">&nbsp;</label>
						<span><input type="submit" name="ssugg" id="ssugg" value="Add New Suggest"/></span>
					</p>
				</form>
			</div>
		</div>
		<?php
	}

	public function render_disqus_config( $shortname, $title, $url, $id = 1) {
		?>
		<script type="text/javascript">
			var disqus_shortname = '<?php echo esc_js( $shortname ); ?>',
				disqus_title = '<?php echo esc_js( $title ); ?>',
				disqus_url = '<?php echo esc_js( $url ); ?>',
				disqus_identifier = '<?php echo esc_js( $shortname ); ?>-<?php echo esc_js( $id ); ?>';
		</script>
		<?php
		if ($this->isMobile): ?>
			<script type="text/javascript">
			(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
			})();
			</script>
			<noscript>
				Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a>
			</noscript>
			<a href="http://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a>
		<?php return; endif;
		
		wp_enqueue_script( 'disqus_embed', 'http://' . $shortname . '.disqus.com/embed.js' );
	}
	
	/**
	 * @param $m
	 * @return array|bool
	 */
	private function get_kbucket_share_data( $m )
	{
		// Exit if kbucket record was not found
		if ( empty( $m ) ) {
			return false;
		}

		$share = array( 'kbucket' => $m );

		$share['url'] = $m->url_kbucket;
		$share['imageUrl'] = $m->image_url;
		$share['title'] = $m->title;
		$share['description'] = strip_tags( $m->description );

		return $share;
	}


	/**
	 * Get categories or subcategories depending from $id_parent argument
	 * Result can be filtered with columns returned
	 * @param int $id_parent
	 * @param bool $columns
	 * @return mixed
	 */
	private function get_subcategories( $id_parent = 0, $columns = false ) {

		if ( ! $columns ) {
			$columns = '`id_cat`,`name`,`level`,`image`,`description`';
		}

		$sql = $id_parent == '0'
			? "SELECT $columns
				FROM `" . $this->wpdb->prefix . "kb_category`
				WHERE `parent_cat`=''
					AND `id_cat` IN
						(
							SELECT `parent_cat`
							FROM `" . $this->wpdb->prefix . 'kbucket` a
							INNER JOIN `' . $this->wpdb->prefix . 'kb_category` b ON a.`id_cat` = b.`id_cat`
							WHERE `level`=2
							GROUP BY `parent_cat`
						)'
			: "SELECT $columns
				FROM `" . $this->wpdb->prefix . "kb_category`
				WHERE `parent_cat`='" . esc_sql( $id_parent ) . "'
					AND `id_cat` IN(
						SELECT `id_cat` FROM `" . $this->wpdb->prefix . 'kbucket` GROUP BY `id_cat`
					)';

		$res = $this->wpdb->get_results( $sql, ARRAY_A );

		return $res;
	}

	/**
	 * @param bool $categoryId
	 * @return bool
	 */
	private function get_category_by_id( $categoryId = false )
	{

		return isset( $this->kbPage->menu['categories'][ $categoryId ] )
			? $this->kbPage->menu['categories'][ $categoryId ]
			: false;
	}


	/**
	 * Get subcategory data
	 * @param bool $categoryId
	 * @return bool
	 */
	private function get_subcategory_by_id( $categoryId = false )
	{
		return isset( $this->kbPage->menu['subcategories'][ $categoryId ] )
			? $this->kbPage->menu['subcategories'][ $categoryId ]
			: false;
	}

	/**
	 * @param bool $categoryId
	 * @return bool
	 */
	private function get_first_subcategory( $categoryId = false )
	{

		$subcategory = false;
		$category = false;

		if ( $categoryId && ! empty( $this->kbPage->menu['categories'][ $categoryId ] ) ) {
			$category = $this->kbPage->menu['categories'][ $categoryId ];
		} elseif ( ! empty( $this->kbPage->menu['categories'] ) ) {
			reset( $this->kbPage->menu['categories'] );
			$categoryIndex = key( $this->kbPage->menu['categories'] );
			$category = $this->kbPage->menu['categories'][ $categoryIndex ];
		}

		if ( ! empty( $category->subcategories ) ) {
			reset( $category->subcategories );
			$sIndex = key( $category->subcategories );
			$subcategoryId = $category->subcategories[ $sIndex ];
			if ( ! empty( $subcategoryId ) ) {
				$subcategory = $this->kbPage->menu['subcategories'][ $subcategoryId ];
			}
		}

		return $subcategory;
	}

	/**
	 * Get the class object instance
	 */
	public static function get_kbucket_instance() {

		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Get Kbuckets records by where criteria
	 * @param $where
	 * @return mixed -numerically indexed array of row objects.
	 */
	private function get_kbuckets( $where )
	{
		$sql = 'SELECT a.`id_kbucket`,
					a.`id_cat`,
					a.`title`,
					a.`description`,
					a.`link`,
					a.`author`,
					a.`add_date`,
					`tags`,
					a.`image_url`,
					a.`short_url`,
					a.`post_id`,
					c.name AS categoryName,
					a.`url_kbucket`
				FROM `' . $this->wpdb->prefix . 'kbucket` a
				LEFT JOIN
				(
					SELECT a.`id_kbucket`,GROUP_CONCAT(`name`) as tags
					FROM `' . $this->wpdb->prefix . 'kbucket` a
					LEFT JOIN `' . $this->wpdb->prefix . 'kb_tags` b ON b.`id_kbucket`  = a.`id_kbucket`
					LEFT JOIN `' . $this->wpdb->prefix . "kb_tag_details` c ON c.`id_tag` = b.`id_tag`
					WHERE $where
				) b ON a.`id_kbucket` = b.id_kbucket
				LEFT JOIN `" . $this->wpdb->prefix . "kb_category` c
					ON a.`id_cat`=c.`id_cat`
				WHERE $where";

		return $this->wpdb->get_results( $sql );
	}

	/**
	 * Get Kbucket record by Kbucket id
	 * @param $kid
	 * @return mixed - row object or false on failure
	 */
	public function get_kbucket_by_id( $kid )
	{
		$kid = esc_sql( $kid );

		$kbucket = $this->get_kbuckets( "a.`id_kbucket`='$kid'" );

		return ! empty( $kbucket[0] ) ? $kbucket[0] : false;
	}

	/**
	 * Get tags by current category id
	 * @return array - numerically indexed array of row objects.
	 */
	private function get_category_tags()
	{
		$sql = 'SELECT t.`id_tag`,
						td.`name`,
						k.`title`,
						COUNT(t.`id_tag`) AS mcnt
				FROM `' . $this->wpdb->prefix . 'kbucket` k
				JOIN `' . $this->wpdb->prefix . 'kb_tags` t ON k.`id_kbucket`=t.`id_kbucket`
				JOIN `' . $this->wpdb->prefix . 'kb_tag_details` td ON td.`id_tag`=t.`id_tag`
				WHERE k.`id_cat`=%s
				GROUP BY td.name
				ORDER BY td.name
				LIMIT 0,' . (int) $this->kbPage->settings->main_no_tags;

		$values = array( $this->category->id_cat );

		$query = $this->wpdb->prepare( $sql, $values );

		return $this->wpdb->get_results( $query );
	}

	/**
	 * Get Author tags by current category id
	 * @return array - numerically indexed array of row objects.
	 */
	private function get_author_tags()
	{
		$sql = 'SELECT k.`author`,
		 				(
		 					SELECT COUNT(*)
		 					FROM `' . $this->wpdb->prefix . "kb_tags`
							WHERE `status` ='1' AND id_kbucket=k.id_kbucket
						) AS mcnt
				FROM `" . $this->wpdb->prefix . 'kbucket` k
				WHERE k.`id_cat`=%s
				GROUP BY `author`
				ORDER BY `author`
				LIMIT 0 , ' . (int) $this->kbPage->settings->author_no_tags;

		$values = array( $this->category->id_cat );

		$query = $this->wpdb->prepare( $sql, $values );
		return $this->wpdb->get_results( $query );
	}

	/**
	 * Get related tags by active tag name and current category id
	 * @param $activeTagName
	 */
	private function set_related_tags( $activeTagName )
	{
		$sql = 'SELECT t.`id_tag`, d.`name`, k.`title`,k.`id_kbucket`,
				COUNT(t.`id_tag`) AS mcnt
				FROM `' . $this->wpdb->prefix . 'kb_tag_details` d
				JOIN `' . $this->wpdb->prefix . 'kb_tags` t ON t.`id_tag`=d.`id_tag`
				JOIN `' . $this->wpdb->prefix . 'kbucket` k ON k.`id_kbucket`=t.`id_kbucket`
				WHERE k.`id_cat`=%s AND k.`id_kbucket` IN (
					SELECT t.`id_kbucket` FROM `' . $this->wpdb->prefix . 'kb_tag_details` d
					JOIN `' . $this->wpdb->prefix . 'kb_tags` t ON t.id_tag=d.id_tag
					WHERE d.`name`=%s
				)
				GROUP BY d.`name`
				ORDER BY d.`name`
				LIMIT 0,'. (int) $this->kbPage->settings->related_no_tags;

		$query = $this->wpdb->prepare( $sql, array( $this->category->id_cat, $activeTagName ) );
		$this->kbPage->relatedTags = $this->wpdb->get_results( $query );
	}

	/**
	* Create Kbuckets listings page URL by multiple params
	* @param $sort
	* @param $tag
	* @return string
	*/
	private function get_current_url( $sort, $tag = false ) {

		$url = ! empty( $this->category ) ? $this->category->url : KBUCKET_URL . '/';

		$params = array();

		$url .= '?';

		if ( isset( $_REQUEST['atag'] ) ) {
			$params[] = 'atag=' . $_REQUEST['atag'];
		}

		if ( 'mtag' == $sort ) {
			if ($tag) {
				return $url . 'mtag=' . $tag;
			}

			if ( isset( $_REQUEST['sort'] ) ) {
				$params[] = 'sort=' . $_REQUEST['sort'] . '&amp;order=' . $_REQUEST['order'];
			}

			return $url . implode( '&amp;', $params );
		}
		
		if ( isset( $_REQUEST['mtag'] ) ) {
			$params[] = 'mtag=' . $_REQUEST['mtag'];
		}

		if ( isset( $_REQUEST['rtag'] ) ) {
			$params[] = 'rtag=' . $_REQUEST['rtag'];
		}

		if ( 'pagin' == $sort ) {
			if ( isset( $_REQUEST['sort'] ) ) {
				$params[] = 'sort=' . $_REQUEST['sort'] . '&amp;order=' . $_REQUEST['order'];
			}
		} else if ( isset( $_REQUEST['sort'] ) ) {
			if ( $sort == $_REQUEST['sort'] ) {
				if ( 'asc' == $_REQUEST['order'] ) {
					$params[] = 'sort=' . $sort . '&amp;order=desc';
				} else {
					$params[] = 'sort=' . $sort . '&amp;order=asc';
				}
			} else {
				$params[] = 'sort=' . $sort . '&amp;order=asc';
			}
		} else {
			$params[] = 'sort=' . $sort . '&amp;order=asc';
		}

		return $url . implode( '&amp;', $params );
	}

	/**
	 * Access Kbucket listing page data
	 * @return stdClass
	 */
	public function get_page_data()
	{
		return $this->kbPage;
	}

	/**
	 * Set mobile layout template if mobile device detected or cookie option has been set
	 */
	public function is_mobile()
	{
		if ( wp_is_mobile() ) {
			$this->isMobile = true;
		} elseif ( isset( $_COOKIE['kb-mobile'] ) && 'y' == $_COOKIE['kb-mobile'] ) {
			$this->isMobile = true;
		}

		if ( isset( $_GET['mobile'] ) ) {
			$switch = $_GET['mobile'];
			setcookie( 'kb-mobile', $switch );
			if ( $switch = 'n' ) {
				$this->isMobile = false;
			}
		}

		if ( ( isset( $_GET['mobile'] ) && 'y' == $_GET['mobile'] ) ) {
			$this->isMobile = true;
		}

		if ( $this->isMobile ) {
	
			$this->template = 'mobile.php';
		}
	}

	/**
	 * Set Kbucket listings page object
	 */
	public function init_kbucket_section()
	{
		define( 'WPKB_PAGE_URL', get_page_link() );

		$this->isKbucketPage = $this->is_kbucket_page();

		if ( $this->isKbucketPage ) {

			//Set menu data
			$sql = 'SELECT c.`id_cat` AS categoryId,
							c.`name` AS categoryName,
							c.`image` AS categoryImage,
							c.`description` AS categoryDescription,
							s.`id_cat` AS subcategoryId,
							s.`name` AS subcategoryName,
							s.`image` AS subcategoryImage,
							s.`description` AS subcategoryDescription,
							s.`parent_cat`
				FROM `' . $this->wpdb->prefix . 'kb_category` c
				LEFT JOIN `' . $this->wpdb->prefix . "kb_category` s
					ON s.`parent_cat`=c.`id_cat`
				WHERE c.`parent_cat`=''";

			$res = $this->wpdb->get_results( $sql );

			$menu = array( 'categories' => array(), 'subcategories' => array() );

			foreach ( $res as $cat ) {
				if ( ! isset( $menu['categories'][ $cat->categoryId ] ) ) {
					$category = new stdClass();
					$category->id_cat = $cat->categoryId;
					$category->url = KBUCKET_URL
						. '/' . sanitize_title_with_dashes( $cat->categoryName )
						. '/kc/' . $cat->categoryId . '/';
					$category->name = $cat->categoryName;
					$category->image = $cat->categoryImage;
					$category->description = $cat->categoryDescription;
					$category->subcategories = array();
					$menu['categories'][ $cat->categoryId ] = $category;
				}

				$subcategory = new stdClass();
				$subcategory->id_cat = $cat->subcategoryId;
				$subcategory->url = KBUCKET_URL
					. '/' . sanitize_title_with_dashes( $cat->categoryName )
					. '/' . sanitize_title_with_dashes( $cat->subcategoryName )
					. '/kc/' . $cat->subcategoryId . '/';
				$subcategory->name = $cat->subcategoryName;
				$subcategory->image = $cat->subcategoryImage;
				$subcategory->description = $cat->subcategoryDescription;
				$subcategory->parent_cat = $cat->parent_cat;

				$menu['categories'][ $cat->categoryId ]->subcategories[] = $cat->subcategoryId;
				$menu['subcategories'][ $cat->subcategoryId ] = $subcategory;
			}

			$this->kbPage->menu = $menu;

			global $wp;

			$requestArr = explode( '/', $wp->request );

			// Kbucket id or category is probably passed at the end of URL
			$keyId = array_pop( $requestArr );

			// Two  possible slugs:
			// kb - kbucket
			// kc - category
			$keySlug = array_pop( $requestArr );

			$categoryId = false;

			// If kb identifier exists,set Kbucket property
			if ( 'kb' == $keySlug ) {
				$this->kbucket = $this->get_kbucket_by_id( $keyId );
				$categoryId = $this->kbucket->id_cat;

			}
			// If kc identifier exists, assign Category id
			elseif ( 'kc' == $keySlug ) {
				$categoryId = $keyId;
			}

			// Read category data
			$this->category = $categoryId ? $this->get_category_by_id( $categoryId ) : false;

			if ( $categoryId && ! $this->category ) {
				$this->category = $this->get_subcategory_by_id( $categoryId );
			}

			$this->kbPage->buckets = array();
			$this->kbPage->pagesCount = 0;
			$this->kbPage->currentPageI = 1;

			if ( ! $this->category ) {
				// Assign first category from existing
				$this->category = $this->get_first_subcategory();
			}

			if ( isset( $this->category->subcategories ) ) {
				$this->category = $this->get_first_subcategory( $categoryId );//dump( $subcategory);
			}

			$this->kbPage->relatedTags = array();
			$this->kbPage->activeTagName = '';
			$this->kbPage->relatedTagName = '';
			$this->kbPage->authorTagName = '';

			// Select by tag name if has been passed
			if ( ! empty( $_REQUEST['mtag'] ) ) {

				$this->kbPage->activeTagName = urldecode( substr( $_REQUEST['mtag'], 0 , 80 ) );

				$this->set_related_tags( $this->kbPage->activeTagName );
			}

			$values = array();

			//Build query to select Kbuckets
			$sql = "SELECT `id_kbucket` AS kbucketId,
						`id_cat`,
						`title`,
						`description`,
						`link`,
						`author`,
						STR_TO_DATE((SUBSTR(`pub_date`, 5, 12) ), '%%d %%b %%Y' ) AS add_date,
						(
							SELECT GROUP_CONCAT(CONCAT(t.`id_tag`, '|', td.`name`) SEPARATOR '~' )
							FROM `" . $this->wpdb->prefix . 'kb_tags` t
							JOIN `' . $this->wpdb->prefix . 'kb_tag_details` td
								ON td.`id_tag`=t.`id_tag`
							WHERE t.`id_kbucket` = kbucketId
						) AS tags
					FROM `' . $this->wpdb->prefix . 'kbucket`
					WHERE ';

			$values[] = $this->category->id_cat;

			$where = "`status`='1' AND `id_cat`=%s ";

			// Select by author name if has been passed
			if ( ! empty( $_REQUEST['atag'] ) ) {
				$this->kbPage->authorTagName  = urldecode( substr( $_REQUEST['atag'], 0, 60 ) );
				$where .= ' AND (`author_alias`=%s OR `author`=%s) ';
				$values[] = $this->kbPage->authorTagName;
				$values[] = $this->kbPage->authorTagName;
			}

			if ( $this->kbPage->activeTagName !== '' ) {
				$tagIds[] = $this->kbPage->activeTagName;
				$where .= 'AND `id_kbucket`
						IN (
							SELECT t.`id_kbucket` FROM `' . $this->wpdb->prefix . 'kb_tags` t
							JOIN `' . $this->wpdb->prefix . 'kb_tag_details` td
								ON td.`id_tag`=t.`id_tag`
							WHERE td.`name`=%s)';

				$values[] = $this->kbPage->activeTagName;

				// Select by active and related tag name if both has been passed
				if ( ! empty( $_REQUEST['rtag'] ) ) {

					$this->kbPage->relatedTagName = urldecode( substr( $_REQUEST['rtag'], 0 , 80 ) );

					$where .= ' AND `id_kbucket`
						IN (
							SELECT t.`id_kbucket` FROM `' . $this->wpdb->prefix . 'kb_tags` t
							JOIN `' . $this->wpdb->prefix . 'kb_tag_details` td
								ON td.`id_tag`=t.`id_tag`
							WHERE td.`name`=%s
							)';
					$values[] = $this->kbPage->relatedTagName;
				}
			}

			$sql .= $where;

			$countSql = 'SELECT COUNT(*) FROM `' . $this->wpdb->prefix . 'kbucket` WHERE ' . $where;
			$countQuery = $this->wpdb->prepare( $countSql, $values );
			$kbucketsCount = $this->wpdb->get_var( $countQuery );

			$sortCols = array( 'author', 'add_date', 'title' );

			if ( ! empty( $_REQUEST['sort'] ) && in_array( $_REQUEST['sort'], $sortCols ) ) {
				$sql .= ' ORDER BY `' . $_REQUEST['sort'] . '`';
				if ( isset( $_REQUEST['order'] ) && ( 'asc' == $_REQUEST['order'] || 'desc' == $_REQUEST['order'] ) ) {
					$sql .= ' ' . $_REQUEST['order'];
				}
			} else {
				$sql .= ' ORDER BY `' . $this->kbPage->settings->sort_by . '` ' . $this->kbPage->settings->sort_order;
			}

			$pagin = isset( $_REQUEST['pagin'] ) ? (int) $_REQUEST['pagin'] : 1;

			$start = ( $pagin - 1 ) * $this->kbPage->settings->no_listing_perpage;

			$sql .= " LIMIT $start," . $this->kbPage->settings->no_listing_perpage;

			$query = $this->wpdb->prepare( $sql, $values );
			$this->kbPage->buckets = $this->wpdb->get_results( $query );

			$this->kbPage->pagesCount = ceil( $kbucketsCount / $this->kbPage->settings->no_listing_perpage );

			$this->kbPage->currentPageI = $pagin;
		}

		$this->kbucketCurrentUrl = $this->get_current_url( 'mtag' );
	}

	/**
	 * Build query string for search query
	 * @param $phrase
	 * @param $cols
	 * @param $operands
	 * @return array
	 */
	private function create_search_condition( $phrase, $cols, $operands )
	{
		$words = explode( ' ', $phrase );
		$conditionsArr = array();
		$values = array();
		$likeConditions = array();

		foreach ( $cols as $col ) {
			foreach ( $words as $word ) {
				if ( $word == '' ) {
					continue;
				}
				foreach ( $operands as $operand ) {
					$conditionsArr[] = "`$col`$operand" . ( $operand == 'LIKE' ? " '%%%s%%'" : '%s' );
					$values[] = $word;
				}
			}
			$likeConditions = implode( ' OR ', $conditionsArr );
		}

		return array( 'conditions' => $likeConditions, 'values' => $values );
	}

	/**
	 * Get result notification content for dashboard
	 * @param $result
	 * @return string
	 */
	private function dashboard_get_result_message( $result )
	{
		$msg = '';

		if ( is_string( $result ) ) {
			$result = array( 'error' => array( $result ) );
		}

		foreach ( $result as $type => $messages ) {
			foreach ( $messages as $message ) {
				$msg .= '
				<div class="updated updated-' . $type . '">
					<p><strong>' . $message . '</strong></p>
				</div>';
			}
		}

		return $msg;
	}

	/**
	 * Scrape Kbucket image URL from external site
	 * Trying to get og:image content value from meta tag
	 * @param $url
	 * @return bool|string
	 */
	public function scrape_og_image( $url )
	{
		$content = $this->curl_get_result( $url );

		libxml_use_internal_errors( true );

		$dom = new DOMDocument();

		if ( $content == '' || false == @$dom->loadHTML( $content ) ) {
			return false;
		}

		$xpath = new DOMXPath( $dom );

		$expr = '//meta[@property="og:image"]/@content';

		// Trying to scrape first image from article containing description
		$nodeList = $xpath->query( $expr );

		$node = is_object( $nodeList ) && is_object( $nodeList->item( 0 ) )
			? $nodeList->item( 0 )->nodeValue
			: false;

		return $node;
	}

	/**
	 * Save Kbucket image by URL as WP Post attachment
	 * @param $imageUrl
	 * @return array|bool
	 */
	private function save_post_image( $imageUrl )
	{
		if ( ! class_exists( 'WP_Http' ) ) {
			include_once( ABSPATH . WPINC. '/class-http.php' ); }

		$httpObj = new WP_Http();

		$photo = $httpObj->request( $imageUrl );
		if ( is_object( $photo ) || $photo['response']['code'] != 200 ) {
			return false;
		}

		global $user_login;

		$attachment = wp_upload_bits(
		 $user_login . '.jpg',
		 null,
		 $photo['body'],
		 date( 'Y-m', strtotime( $photo['headers']['last-modified'] ) )
		);

		return $attachment;
	}

	/**
	 * Execute various administrative actions and return result message
	 * @param $action
	 * @return array|string
	 */
	public function run_admin_action( $action ) {

		$success = array();
		$errors = array();
		$warnings = array();

		switch ( $action ) {
			
			case 'updateAdvertisimentCode':
				
				if ( empty( $_POST['kb-ad-code'] ) ) {
					return 'Ad code not specified!';
				}
				
				$adCode = $_POST['kb-ad-code'];
				
				$this->wpdb->update(
					 $this->wpdb->prefix . 'kb_page_settings',
					 array(
						 'advertisiment_code' => $adCode,
					 )
				);
				
				break;
			
			case 'uploadXml':

				//  Init session if not active
				if (!session_id()) {
					if (!session_start()) {
						return "Cannot start a session! Check your server settings";
					}
				}
				
				if ( ! is_writable( WPKB_PATH . 'uploads' ) ) {
					return 'Upload directory: ' . WPKB_PATH . 'uploads not writable! Check the permissions';
				}

				if ( ! empty( $_FILES[' xml']['error'] )  || 0 !== $_FILES['upload_xml']['error'] ) {
					return 'Cannot upload the file';
				}

				$uploadFile = WPKB_PATH . 'uploads/kbucket.xml';

				if ( false == @move_uploaded_file( $_FILES['upload_xml']['tmp_name'], $uploadFile ) ) {
					return 'Cannot move uploaded file: ' . $uploadFile;
				}

				if ( ! is_file( $uploadFile ) ) {
					return 'Cannot get uploaded file: ' . $uploadFile;
				}

				libxml_use_internal_errors( true );

				$doc = new DOMDocument();

				if ( empty( $doc ) ) {
					return 'Cannot create DOMDocument object.';
				}

				$res = $doc->load( $uploadFile );

				if ( false == $res ) {
					return 'Failed to load file into DOM object';
				}

				//$library = $doc->getElementsByTagName("LIBRARY");
				//$comment = $doc->getElementsByTagName("COMMENT");
				//$comment = is_object( $comment->item(0) ) ? $comment->item(0)->nodeValue : '';
				$bookArr = $doc->getElementsByTagName( 'BOOK' );

				/*$sql = 'INSERT INTO `' . $this->wpdb->prefix . 'author_alias`
						(
							SELECT `author`, `author_alias`
							FROM `' . $this->wpdb->prefix . "kbucket`
							WHERE `author_alias` <> ''
							GROUP BY `author`, `author_alias`
						)";

				$this->wpdb->query( $sql );

				$sql = 'INSERT INTO `' . $this->wpdb->prefix . 'set_social`
						(
							SELECT name, description, image
							FROM `' . $this->wpdb->prefix . "kb_category`
							WHERE `description` <> '' OR `image` <> ''
							GROUP BY `name`, `description`,`image`
						)";

				$this->wpdb->query( $sql );*/

				$truncateTables = array(
					$this->wpdb->prefix . 'kb_category',
					$this->wpdb->prefix . 'kbucket',
					$this->wpdb->prefix . 'kb_tags',
					$this->wpdb->prefix . 'kb_tag_details',
					$this->wpdb->prefix . 'author_alias',
				);

				foreach ( $truncateTables as $table ) {
					$this->wpdb->query( "TRUNCATE `$table`" );
				}

				$pagesUrls = array();

				$kbucketsSql = 'INSERT IGNORE INTO `' . $this->wpdb->prefix . 'kbucket`
								(
									`id_kbucket`,
									`id_cat`,
									`title`,
									`description`,
									`link`,
									`author`,
									`author_alias`,
									`pub_date`,
									`add_date`,
									`url_kbucket`
								)
								VALUES ';

				$kbucketsPlaceholders = array();
				$kbucketsValues = array();

				$categoriesSql = 'INSERT IGNORE INTO `' . $this->wpdb->prefix . 'kb_category`
								(
									`id_cat`,
									`name`,
									`parent_cat`,
									`level`,
									`add_date`
								) VALUES ';

				$categoriesPlaceholders = array();
				$categoriesValues = array();

				$tagsDetailsSql = 'INSERT IGNORE INTO `' . $this->wpdb->prefix . 'kb_tag_details`
							(`name`)
							VALUES ';

				$tagsDetailsPlaceholders = array();
				$tagsDetailsValues = array();

				$tags = array();

				// Iterate all book elements one by one
				foreach ( $bookArr as $book ) {

					$categoriesPlaceholders[] = '(%s,%s,%s,%d,%s)';

					/** @var $book object */
					$attrBook_Name = $book->getAttribute( 'name' );
					$attrBook_UID = $book->getAttribute( 'object_uid' );
					//$attrBook_caption = $book->getAttribute( 'caption' );
					//$attrBook_tooltip = $book->getAttribute( 'tooltip' );

					$categoriesValues[] = $attrBook_UID;
					$categoriesValues[] = $attrBook_Name;
					$categoriesValues[] = '';
					$categoriesValues[] = 1;
					$categoriesValues[] = date( 'Y' ) . '-' . date( 'm' ) . '-' . date( 'd' );

					$parentCategoryId = wp_create_category( $attrBook_Name );

					/**
					 * @var $comment object
					 * @var $commentChapter object
					 * @var $chapter object
					 * @var $page object
					 * @var $commentPageNode object
					 * @var $urlPageNode object
					 */

					//$comment = $book->getElementsByTagName("COMMENT");
					//$comment = is_object( $comment->item(0) ) ? $comment->item(0)->nodeValue : '';
					$chapterArr = $book->getElementsByTagName( 'CHAPTER' );

					foreach ( $chapterArr as $chapter ) {

						$categoriesPlaceholders[] = '(%s,%s,%s,%d,%s)';

						$attrChapter_Name = $chapter->getAttribute( 'name' );
						$attrChapter_UID = $chapter->getAttribute( 'object_uid' );
						//$attrChapter_caption = $chapter->getAttribute( 'caption' );
						//$attrChapter_tooltip = $chapter->getAttribute( 'tooltip' );
						//$commentChapter = $chapter->getElementsByTagName("COMMENT");
						//$commentChapter = is_object( $commentChapter->item(0) ) ? $commentChapter->item(0)->nodeValue : '';

						// Add subcategories values
						$categoriesValues[] = $attrChapter_UID;
						$categoriesValues[] = $attrChapter_Name;
						$categoriesValues[] = $attrBook_UID;
						$categoriesValues[] = 2;
						$categoriesValues[] = date( 'Y' ) . '-' . date( 'm' ) . '-' . date( 'd' );

						wp_create_category( $attrChapter_Name, $parentCategoryId );

						$pageArr = $chapter->getElementsByTagName( 'PAGE' );

						foreach ( $pageArr as $page ) {

							$attrPage_Name = $page->getAttribute( 'name' );
							$attrPage_UID = $page->getAttribute( 'object_uid' );
							//$attrPage_caption = $page->getAttribute( 'caption' );
							$attrPage_tooltip = $page->getAttribute( 'tooltip' );
							$attrPage_author = trim( $page->getAttribute( 'author' ) );
							$attrPage_pub_date = $page->getAttribute( 'pubDate' );

							$commentPageNode = $page->getElementsByTagName( 'COMMENT' );
							$commentPageText = isset( $commentPageNode->item( 0 )->nodeValue )
								? $commentPageNode->item( 0 )->nodeValue
								: '';

							$urlPageNode = $page->getElementsByTagName( 'PAGEURL' );

							$urlPage = isset( $urlPageNode->item( 0 )->nodeValue )
								? $urlPageNode->item( 0 )->nodeValue
								: '';

							$pagesUrls[] = array(
								'url' => $urlPage,
								'name' => $attrPage_Name,
							);

							$kbucketsPlaceholders [] = '(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)';

							$kbucketsValues[] = $attrPage_UID;
							$kbucketsValues[] = $attrChapter_UID;
							$kbucketsValues[] = $attrPage_Name;
							$kbucketsValues[] = $commentPageText;
							$kbucketsValues[] = $urlPage;
							$kbucketsValues[] = $attrPage_author;
							$kbucketsValues[] = '';
							$kbucketsValues[] = $attrPage_pub_date;
							$kbucketsValues[] = date( 'Y' ) . '-' . date( 'm' ) . '-' . date( 'd' );

							$kbucketUrl = 'kbucket/' . sanitize_title_with_dashes( $attrBook_Name )
										. '/' . sanitize_title_with_dashes( $attrChapter_Name )
										. '/' . sanitize_title_with_dashes( $attrPage_Name )
										. '/kb/' . $attrPage_UID;

							$kbucketsValues[] = $kbucketUrl;

							$tagArr = explode( ',', $attrPage_tooltip );

							// Iterate tags
							foreach ( $tagArr as $tag ) {

								$tag = strtolower( trim( $tag ) );

								$tagsDetailsPlaceholders[] = '(%s)';
								$tagsDetailsValues[] = $tag;

								if ( ! isset( $tags[ $tag ] ) ) {
									$tags[ $tag ] = array();
								}

								$tags[ $tag ][] = $attrPage_UID;
							}
						}
					}
				}

				$resMsg = '';

				$categoriesSql .= implode( ',', $categoriesPlaceholders );
				$categoriesSql = $this->wpdb->prepare( $categoriesSql, $categoriesValues );
				if ( $categoriesSql ) {
					$resMsg .= 'Inserted Categories: ' . $this->wpdb->query( $categoriesSql ) . '<br/>';
				}

				$kbucketsSql .= implode( ',', $kbucketsPlaceholders );
				$kbucketsSql = $this->wpdb->prepare( $kbucketsSql, $kbucketsValues );
				if ( $kbucketsSql ) {
					$resMsg .= 'Inserted Kbuckets: ' . $this->wpdb->query( $kbucketsSql ) . '<br/>';
				}

				$tagsDetailsSql .= implode( ',', $tagsDetailsPlaceholders );
				$tagsDetailsSql = $this->wpdb->prepare( $tagsDetailsSql, $tagsDetailsValues );
				if ( $tagsDetailsSql ) {
					$resMsg .= 'Inserted Tags Details: ' . $this->wpdb->query( $tagsDetailsSql ) . '<br/>';

					$tagsSql = 'INSERT IGNORE INTO `' . $this->wpdb->prefix . 'kb_tags`
								(
									`id_kbucket`,
									`id_tag`
								) VALUES';

					$tagsPlaceholders = array();
					$tagsValues = array();

					$savedTagsDetails = $this->wpdb->get_results(
					 'SELECT `id_tag`,`name`
					 FROM `' . $this->wpdb->prefix . 'kb_tag_details`'
					);

					// Add values for category tags
					foreach ( $savedTagsDetails as $tag ) {
						if ( isset( $tags[ $tag->name ] ) ) {
							foreach ( $tags[ $tag->name ] as $kbucketId ) {
								$tagsPlaceholders[] = '(%s,%d)';
								$tagsValues[] = $kbucketId;
								$tagsValues[] = $tag->id_tag;
							}
						}
					}

					if ( ! empty( $tagsValues ) ) {

						$tagsSql .= implode( ',', $tagsPlaceholders );
						$tagsSql = $this->wpdb->prepare( $tagsSql, $tagsValues );
						if ( $tagsSql ) {
							$resMsg .= 'Inserted Tags: ' . $this->wpdb->query( $tagsSql ) . '<br/>';
						}
					}
				}

				$sql = 'UPDATE `' . $this->wpdb->prefix . 'kbucket` a
						SET a.author_alias = ifnull(
							(
								SELECT b.alias
								FROM `' . $this->wpdb->prefix .  'author_alias` b
								WHERE a.author = b.author
							), a.author_alias
						)';

				$this->wpdb->query( $sql );

				/*$sql = 'UPDATE `' . $this->wpdb->prefix . 'kb_category` a
				 		SET a.`description` = ifnull(
								(
									SELECT b.`description`
									FROM `' . $this->wpdb->prefix . 'set_social` b
									WHERE a.`name` = b.`name`
								),a.`description`
								),
							a.`image` = ifnull(
							(
								SELECT b.`image`
								FROM `' . $this->wpdb->prefix . 'set_social` b
								WHERE a.`name` = b.`name`
							),a.`image`
						)';
				$this->wpdb->query( $sql );*/

				$success[] = 'KBuckets uploaded successfully!<br/>' . $resMsg;
				break;
		}
		return array( 'success' => $success, 'error' => $errors, 'warning' => $warnings );
	}

	/**
	 * Dashboard functionality for Kbucket management
	 */
	public function render_dashboard() {

		$amsg = ''; ?>
	<h1>Kbucket</h1>
	<?php
	if ( isset( $_REQUEST['submit'] ) ) {
		
		$submit = $_REQUEST['submit'];

		switch ( $submit ) {

			// Update tag
			case 'Update Tag':
				$this->wpdb->update(
				 $this->wpdb->prefix . 'kbucket',
				 array( 'author_alias' => $_REQUEST['tag_name'] ),
				 array( 'author' => $_REQUEST['author'] )
				);
				$amsg = 'Auther alias name has been successfully updated';
				break;

			// Delete Author
			case 'Delete Author':
				$this->wpdb->delete(
				 $this->wpdb->prefix . 'kbucket',
				 array( 'author' => $_REQUEST['author'] )
				);
				$amsg = 'Auther data have been successfully deleted';
				break;

			// Import Kbuckets via XML upload
			case 'Upload XML':
				$res = $this->run_admin_action( 'uploadXml' );
				$amsg = $this->dashboard_get_result_message( $res );
				break;
				
			// Update Advertisiment code
			case 'Save Ad code':
				$res = $this->run_admin_action( 'updateAdvertisimentCode' );
				$amsg = $this->dashboard_get_result_message( $res );
				break;

			// Add Category
			case 'Add Category':

				$data = array(
					'name' => $_REQUEST['category'],
					'add_date' => date( 'Y-m-d' ),
				);

				if ( $_REQUEST['parent_category'] != '' ) {
					$parCatArr = explode('#', $_REQUEST['parent_category'] );

					$data['parent_cat'] = $parCatArr[0];
					$data['level'] = $parCatArr[1] + 1;

					$esc = array(
						'%s',
						'%s',
						'$s',
						'%d',
					);
				} else {
					$data['level'] = 1;

					$esc = array(
						'%s',
						'%s',
						'%d'
					);
				}

				$this->wpdb->insert(
				 $this->wpdb->prefix . 'kb_category',
				 $data,
				 $esc
				);
				break;

			// Add Settings
			case 'Apply Settings':
				$this->wpdb->insert(
				 $this->wpdb->prefix . 'kb_page_settings',
				 array(
					'sort_by' => $_REQUEST['sortBy'],
					'sort_order' => $_REQUEST['sortOrder'],
					'no_listing_perpage' => $_REQUEST['no_listing_page'],
					'main_font_min' => $_REQUEST['main_font_min'],
					'main_font_max' => $_REQUEST['main_font_max'],
					'main_no_tags' => $_REQUEST['no_main_tags'],
					'author_font_min' => $_REQUEST['author_font_min'],
					'author_font_max' => $_REQUEST['author_font_max'],
					'author_no_tags' => $_REQUEST['no_author_tags'],
					'related_font_min' => $_REQUEST['related_font_min'],
					'related_font_max' => $_REQUEST['related_font_max'],
					'related_no_tags' => $_REQUEST['no_related_tags'],
				 )
				);
				break;

			// Update Settings
			case 'Update Settings':
				$this->wpdb->update(
				 $this->wpdb->prefix . 'kb_page_settings',
				 array(
					'sort_by' => $_REQUEST['sortBy'],
					'sort_order' => $_REQUEST['sortOrder'],
					'no_listing_perpage' => $_REQUEST['no_listing_page'],
					'main_no_tags' => $_REQUEST['no_main_tags'],
					'author_no_tags' => $_REQUEST['no_author_tags'],
					'related_no_tags' => $_REQUEST['no_related_tags'],
					'author_tag_display' => $_REQUEST['atd'],
					'main_tag_color1' => $_REQUEST['main_tag_color1'],
					'main_tag_color2' => $_REQUEST['main_tag_color2'],
					'author_tag_color1' => $_REQUEST['author_tag_color1'],
					'author_tag_color2' => $_REQUEST['author_tag_color2'],
					'related_tag_color1' => $_REQUEST['related_tag_color1'],
					'related_tag_color2' => $_REQUEST['related_tag_color2'],
					'header_color' => $_REQUEST['header_color'],
					'author_color' => $_REQUEST['author_color'],
					'content_color' => $_REQUEST['content_color'],
					'tag_color' => $_REQUEST['tag_color'],
					'page_title' => $_REQUEST['page_title'],
					'site_search' => $_REQUEST['site_search'],
					'm1' => $_REQUEST['m1'],
					'm2' => $_REQUEST['m2'],
					'm3' => $_REQUEST['m3'],
					'm4' => $_REQUEST['m4'],
					'a1' => $_REQUEST['a1'],
					'a2' => $_REQUEST['a2'],
					'a3' => $_REQUEST['a3'],
					'a4' => $_REQUEST['a4'],
					'r1' => $_REQUEST['r1'],
					'r2' => $_REQUEST['r2'],
					'r3' => $_REQUEST['r3'],
					'r4' => $_REQUEST['r4'],
				 ),
				 array(
					'id_page' => 1,
				 )
				);
				break;

			// Update Category
			case 'Update':

				foreach ( $_REQUEST['description'] as $i => $categoryDescription ) {

					$err = '';
					$file = $_FILES['upload_file']['name'][ $i ]; // @TODO: Sanitize file name
					$uploadPath = WPKB_PATH . 'uploads/categories';
					$finalFile = $uploadPath . '/' . $file;
					$uploadError = $_FILES['upload_file']['error'][ $i ];
					$values = array();

					if ( !empty( $file ) ):
						if ( !is_writable( $uploadPath ) ) :
							$err = 'Upload directory: ' . $uploadPath . ' is not writable';
						elseif ( file_exists( $finalFile ) ):
							$err = 'Upload file already exists!';
							$values['image'] = $file;
						// Is upload file?
						elseif ( $uploadError > 0 && $uploadError !== 4 ):
								switch ( $uploadError ) {
									case 1:
										$err = 'The uploaded file exceeds the upload_max_filesize';
										break;
									case 2:
										$err = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
										break;
									case 3:
										$err = 'The uploaded file was only partially uploaded';
										break;
									case 6:
										$err = 'Missing a temporary folder';
										break;
									case 7:
										$err = 'Failed to write file to disk';
										break;
									case 8:
										$err = 'A PHP extension stopped the file upload';
										break;
									default:
										$err = 'Unexpected upload error';
								}
								$err = 'Upload error: ' . $err . '</p>';
						elseif ( !move_uploaded_file( $_FILES['upload_file']['tmp_name'][ $i ], $finalFile ) ):
							$err = 'Unexpected upload error for file: ' . $file . '<br/>' . $finalFile;
						else:
							$values['image'] = $file;
						endif;
					endif; 

					if ( $categoryDescription != '' ) {
						$values['description'] = $categoryDescription;
					}

					// No action if no upload image and description
					if ( empty( $values ) ) {
						continue;
					}

					$this->wpdb->update(
					 $this->wpdb->prefix . 'kb_category',
					 $values,
					 array(
						'id_cat' => $_REQUEST['idArr'][ $i ],
					 )
					);

					if ( $err !== '' ): ?>
						<div class="updated updated-error">
							<p><strong><?php echo $err; ?></strong></p>
						</div>
					<?php endif;
				}
				break;
		}
	}

		echo $amsg; ?>

		<ul id="kBucketTabs" class="shadetabs">
			<li><a href="#" rel="country1" class="selected">Settings</a></li>
			<li><a href="#" rel="country2" class="selected">Upload Kbuckets</a></li>
			<li><a href="#" rel="country3">Set Social</a></li>
			<?php /*<li><a href="#" rel="country4">Tag Clouds</a></li>*/ ?>
			<li><a href="#" rel="country5">Suggested</a></li>
			<li><a href="#" rel="country6" class="selected">Kbuckets</a></li>
			<li><a href="#" rel="country7" class="selected">Ad code</a></li>
		</ul>
		<div id="messages"></div>
		<div class="kbucket-tabcontent">
		<div id="country1" class="tabcontent">
			<form method="post" id="form" enctype="multipart/form-data">
				<?php $sql_sel = 'SELECT *
								FROM ' . $this->wpdb->prefix . "kb_page_settings
								WHERE id_page='1'";
				$thepost = $this->wpdb->get_row( $sql_sel );
				?>
				<table>
				<tr>
					<th colspan="3">
						Page Settings
					</th>
				</tr>
				<tr>
					<td class="kbucket-td-w30">Author Tag Display</td>
					<td class="kbucket-td-w5">:</td>
					<td class="kbucket-td-w60">
						<input type="radio" name="atd" id="atd-y" value="1" <?php
						if ( 1 == $thepost->author_tag_display ) {
							echo 'checked="checked"';
						}
						?> /><label for="atd-y">Yes</label>
						<input type="radio" name="atd" id="atd-n" value="0" <?php
						if ( 0 == $thepost->author_tag_display ) {
							echo 'checked="checked"';
						}
						?> /><label for="atd-n">No &nbsp;</label>
					</td>
				</tr>
				<tr>
					<td class="kbucket-td-w30">Page Title</td>
					<td class="kbucket-td-w5">:</td>
					<td class="kbucket-td-w60">
						<input type="text" name="page_title" id="page_title" value="<?php
							echo $thepost->page_title;
						?>"/>
						<label for="page_title">If blank, empty title</label>
					</td>
				</tr>
				<tr>
					<td class="kbucket-td-w30">Site Search</td>
					<td class="kbucket-td-w5">:</td>
					<td class="kbucket-td-w60">
						<input type="radio" name="site_search" id="site_search-1" value="1" <?php
						if ( 1 == $thepost->site_search ) {
							echo 'checked="checked"';
						} ?> /><label for="site_search-1">Yes</label>
						<input type="radio" name="site_search" id="site_search-0" value="0" <?php
						if ( 0 == $thepost->site_search ) {
							echo 'checked="checked"';
						} ?> /><label for="site_search-0">No &nbsp;</label>
					</td>
				</tr>
				<tr>
					<td class="kbucket-td-w30">Sort By</td>
					<td class="kbucket-td-w5">:</td>
					<td class="kbucket-td-w60">
						<select name="sortBy" id="sortBy">
							<option value="">Select</option>
							<option
								value="author" <?php
								if ( 'author' == $thepost->sort_by ) {
									?> selected="selected"<?php
								} ?>>
								Author
							</option>
							<option value="title" <?php
							if ( 'title' == $thepost->sort_by ) {
								?>selected="selected"<?php
							} ?>>
								Title
							</option>
							<option value="add_date"
							<?php if ( 'add_date' == $thepost->sort_by ) { ?>selected="selected"<?php } ?>>
								Date
							</option>
						</select>
					</td>
				</tr>
				<tr>
					<td align="left" height="40">Sort Order</td>
					<td>:</td>
					<td align="left">
						<select name="sortOrder" id="sortOrder">
							<option value="">Select</option>
							<?php if ( isset( $thepost->sort_order ) ) { ?>
								<option value="asc" <?php if ( 'asc' == $thepost->sort_order ) { ?>
									selected="selected"<?php } ?>>Ascending
								</option>
								<option value="desc"<?php if ( 'desc' == $thepost->sort_order ) { ?>
									selected="selected"<?php } ?>>Descending
								</option>
								<option value="desc"<?php if ( 'desc' == $thepost->sort_order ) { ?>
									selected="selected"<?php } ?>>Newest
								</option>
								<option value="asc"<?php if ( 'asc' == $thepost->sort_order ) { ?>
									selected="selected"<?php } ?>>Oldest
								</option>
							<?php } else { ?>
								<option value="asc">Ascending</option>
								<option value="desc">Descending</option>
								<option value="desc">Newest</option>
								<option value="asc">Oldest</option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td align="left" height="40">Number Of Listing per Page</td>
					<td>:</td>
					<td align="left">
						<?php if ( isset( $thepost->no_listing_perpage ) ): ?>
						<input type="text" id="no_listing_page" name="no_listing_page" value="<?php
							echo $thepost->no_listing_perpage;
						?>" class="number" />
					<?php else : ?>
						<input type="textbox" id="no_listing_page" name="no_listing_page" value="10" class="number"/>
					<?php endif; ?>
					</td>
				</tr>
				<tr>
					<th height="50" colspan="3">
						Tag Cloud Settings
					</th>
				</tr>
				<tr>
					<th class="kbucket-h30" colspan="3" align="left">
						Main Tag Block
					</th>
				</tr>
				<tr>
					<td align="left" height="40">Tag Cloud Font Sizes</td>
					<td>:</td>
					<td class="kbucket-text-left">
						<table>
							<tr>
								<td>1-3</td>
								<td>3 -7</td>
								<td>7-15</td>
								<td>15+</td>
							</tr>
							<tr>
								<td><input type="text" class="kbucket-input50" name="m1" value="<?php
									echo $thepost->m1;
								?>"/></td>
								<td><input type="text" class="kbucket-input50" name="m2" value="<?php
									echo $thepost->m2; ?>"/></td>
								<td><input type="text" class="kbucket-input50" name="m3" value="<?php
									echo $thepost->m3; ?>"/></td>
								<td><input type="text" class="kbucket-input50" name="m4" value="<?php
									echo $thepost->m4; ?>"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="kbucket-text-left-h40">Number Of Tags To Be Displayed</td>
					<td>:</td>
					<td class="kbucket-text-left">
						<?php if ( isset( $thepost->main_no_tags ) ): ?>
						<input type="text" id="no_main_tags" name="no_main_tags" value="<?php
							echo $thepost->main_no_tags; ?>" class="number" />
						<?php else : ?>
						<input type="text" name="no_main_tags" id="no_main_tags" value="" class="number"/>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td class="kbucket-text-left-h40">Main Tag Cloud Colors</td>
					<td>:</td>
					<td class="kbucket-text-left">
						<input type="text" name="main_tag_color1" id="main_tag_color1" class="kbucket-input100 colorpickerField" value="<?php echo $thepost->main_tag_color1; ?>"/>
						<input type="text" name="main_tag_color2" id="main_tag_color2" class="kbucket-input100 colorpickerField" value="<?php echo $thepost->main_tag_color2; ?>"/>
					</td>
				</tr>
				<tr>
					<th colspan="3" class="kbucket-text-left kbucket-h30">
						Author Tag Block
					</th>
				</tr>
				<tr>
					<td class="kbucket-text-left-h40">Tag Cloud Font Size</td>
					<td>:</td>
					<td class="kbucket-text-left">
						<table>
							<tr>
								<td>1-3</td>
								<td>3 -7</td>
								<td>7-15</td>
								<td>15+</td>
							</tr>
							<tr>
								<td><input type="text" class="kbucket-input50" name="a1" value="<?php
									echo $thepost->a1; ?>"/></td>
								<td><input type="text" class="kbucket-input50" name="a2" value="<?php
									echo $thepost->a2; ?>"/></td>
								<td><input type="text" class="kbucket-input50" name="a3" value="<?php
									echo $thepost->a3; ?>"/></td>
								<td><input type="text" class="kbucket-input50" name="a4" value="<?php
									echo $thepost->a4; ?>"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="kbucket-text-left-h40">Number Of Tags To Be Displayed</td>
					<td>:</td>
					<td class="kbucket-text-left">
						<?php if ( isset( $thepost->author_no_tags ) ): ?>
							<input type="text" id="no_author_tags" name="no_author_tags"
							value="<?php echo $thepost->author_no_tags; ?>" class="number" />
						<?php else : ?>
							<input type="text" name="no_author_tags" id="no_author_tags" value="" class="number"/>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td class="kbucket-text-left-h40">Author Tag Cloud Colors</td>
					<td>:</td>
					<td class="kbucket-text-left">
						<input type="text" name="author_tag_color1" id="author_tag_color1"
						value="<?php echo $thepost->author_tag_color1; ?>" class="kbucket-input100 colorpickerField"/>
						<input type="text" name="author_tag_color2" id="author_tag_color2"
						value="<?php echo $thepost->author_tag_color2; ?>" class="kbucket-input100 colorpickerField"/>
					</td>
				</tr>
				<tr>
					<th colspan="3" class="kbucket-text-left kbucket-h30">
						Related Tag Block
					</th>
				</tr>
				<tr>
					<td class="kbucket-text-left-h40">Tag Cloud Font Size</td>
					<td>:</td>
					<td class="kbucket-text-left">
						<table>
							<tr>
								<td>1-3</td>
								<td>3 -7</td>
								<td>7-15</td>
								<td>15+</td>
							</tr>
							<tr>
								<td><input type="text" class="kbucket-input50" name="r1" value="<?php
									echo $thepost->r1; ?>"/></td>
								<td><input type="text" class="kbucket-input50" name="r2" value="<?php
									echo $thepost->r2; ?>"/></td>
								<td><input type="text" class="kbucket-input50" name="r3" value="<?php
									echo $thepost->r3; ?>"/></td>
								<td><input type="text" class="kbucket-input50" name="r4" value="<?php
									echo $thepost->r4; ?>"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="kbucket-text-left-h40">Number Of Tags To Be Displayed</td>
					<td>:</td>
					<td class="kbucket-text-left">
						<?php if ( isset( $thepost->related_no_tags ) ): ?>
							<input type="text" id="no_related_tags" name="no_related_tags"
							value="<?php echo $thepost->related_no_tags; ?>" class="number" />
						<?php else : ?>
							<input type="text" name="no_related_tags" id="no_related_tags" value="" class="number"/>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td class="kbucket-text-left-h40">Realted Tag Cloud Colors</td>
					<td>:</td>
					<td class="kbucket-text-left">
						<input type="text" name="related_tag_color1" id="related_tag_color1"
						value="<?php echo $thepost->related_tag_color1; ?>" class="kbucket-input100 colorpickerField"/>
						<input type="text" name="related_tag_color2" id="related_tag_color2"
						value="<?php echo $thepost->related_tag_color2; ?>" class="kbucket-input100 colorpickerField"/>
					</td>
				</tr>
				<tr>
					<th colspan="3" class="kbucket-text-left kbucket-h30">
						Content Color
					</th>
				</tr>
				<tr>
					<td class="kbucket-text-left-h40">Header Color</td>
					<td>:</td>
					<td class="kbucket-text-left"><input class="kbucket-input100 colorpickerField" type="text" name="header_color" id="header_color"
					value="<?php echo $thepost->header_color; ?>"/></td>
				</tr>
				<tr>
					<td class="kbucket-text-left-h40">Author Color</td>
					<td>:</td>
					<td class="kbucket-text-left"><input class="kbucket-input100 colorpickerField" type="text" name="author_color" id="author_color"
					value="<?php echo $thepost->author_color; ?>"/></td>
				</tr>
				<tr>
					<td class="kbucket-text-left-h40">Content Color</td>
					<td>:</td>
					<td class="kbucket-text-left"><input type="text" class="kbucket-input100 colorpickerField" name="content_color" id="content_color"
					value="<?php echo $thepost->content_color; ?>"/></td>
				</tr>
				<tr>
					<td class="kbucket-text-left-h40">Tags Color</td>
					<td>:</td>
					<td class="kbucket-text-left"><input type="text" class="kbucket-input100 colorpickerField" name="tag_color" id="tag_color"
					value="<?php echo $thepost->tag_color; ?>"/></td>
				</tr>
				<tr>
					<td class="kbucket-text-left-h40" colspan="3">
						<?php if ( isset( $thepost->related_font_max ) ): ?>
							<input type="submit" name="submit" id="submit" value="Update Settings"/>
						<?php else : ?>
						<input type="submit" name="submit" id="submit" value="Update Settings"/>
						<?php endif; ?>
					</td>
				</tr>
			</table>
			</form>
		</div>

		<div id="country2" class="tabcontent">
			<form method="post" id="form" enctype="multipart/form-data">
				<div style="width:700px;height:200px; margin-top:10px;">

					<input type="file" size="24" name="upload_xml" id="upload_xml"/>
					<input type="submit" name="submit" value="Upload XML"/>
				</div>
			</form>
		</div>

		<div id="country3" class="tabcontent">
			<div style="width:700px;height:auto; margin-top:10px; font-family:Arial, Helvetica, sans-serif">
				<form method="post" id="form" enctype="multipart/form-data">
					<table width="700" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<th width="180" class="kbucket-text-left" height="35">Categories</th>
							<th width="250">Description</th>
							<th width="250"> Images</th>
						</tr>
						<?php print_r( $this->get_categories_dropdown( 0 ) ); ?>
						<tr>
							<td colspan="3"><input type="submit" name="submit" value="Update"></td>
						</tr>
					</table>

					<table width="500" cellspacing="0" cellpadding="0" style="display:none;">
						<tr>
							<th colspan="3" height="40">Add New Categories</th>
						</tr>
						<tr>
							<td class="kbucket-text-left-h40" width="45%">Parent Category<br/>
								[<span style="color:red">Select Parent Category If Exists</span>]
							</td>
							<td width="5%">:</td>
							<td class="kbucket-text-left" width="50%">
								<select name="parent_category">
									<option value="">Select Parent category</option>
									<?php $sql_sel = 'SELECT id_cat,name,level
													FROM ' . $this->wpdb->prefix . 'kb_category ';
									$categories = $this->wpdb->get_results( $sql_sel );
									foreach ( $categories as $c ) { ?>
									<option value="<?php echo esc_attr( $c->id_cat ) . '#' . esc_attr( $c->level ); ?>">
										<?php echo ucfirst( esc_html( $c->name ) ); ?>
										</option><?php
									}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="kbucket-text-left-h40">Category Name</td>
							<td>:</td>
							<td class="kbucket-text-left"><input type="text" name="category" value=""/></td>
						</tr>
						<tr>
							<td colspan="3" height="40">
								<input type="submit" name="submit" value="Add Category"/>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>

		<div id="country5" class="tabcontent">
			<div style="width:700px;margin-top:10px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
				<a href="<?php echo WPKB_PLUGIN_URL . '/suggest_rss.php'; ?>" target="_blank"><img
						src="<?php echo WPKB_PLUGIN_URL; ?>/images/rss_icon.png"/></a>
				<table>
					<?php
					if ( isset( $_REQUEST['delsug'] ) ) {
						$sqldel = 'delete FROM ' . $this->wpdb->prefix . 'kb_suggest WHERE id_sug = '
							. $_REQUEST['delsug'];
						$this->wpdb->query( $sqldel ); ?>
						<tr>
							<td colspan=2 style=" background-color: #FFFBCC;border:1px solid pink;color: #555555; width:500px; text-align:center; padding:5px;">
								Suggested content has been deleted successfully...
							</td>
						</tr><?php
					}

					$data = $this->wpdb->get_results(
					 "SELECT a.*,b.* ,DATE_FORMAT(a.`add_date`,'%d-%m-%Y' ) AS pubdate
					 FROM `" . $this->wpdb->prefix . 'kb_suggest` a
					 LEFT JOIN
					 (
						SELECT c.`id_cat` AS parid,
								b.`id_cat` AS subid,
								c.`name` AS parcat,
								b.`name` AS subcat,
								b.`description` AS subdes,
								c.`description` AS pades
						FROM `' . $this->wpdb->prefix . 'kb_category` b
						INNER JOIN `' . $this->wpdb->prefix . 'kb_category` c ON c.`id_cat` = b.`parent_cat`
							 ) b ON a.`id_cat` = b.`subid`'
					);

					foreach ( $data as $item ): ?>
						<tr>
							<td class="kbucket-text-right"><strong>Suggested for:</strong></td>
						<td>
							<span class="generic_text_3">
								<span style="text-transform: uppercase;"><?php echo esc_html( $item->subcat ); ?></span>
							</span>
						</td>
					</tr>
					<tr>
						<td class="kbucket-text-right"><strong>Date:</strong></td>
						<td><?php echo esc_html( $item->add_date ); ?></td>
					</tr>
					<tr>
						<td class="kbucket-text-right"><strong>Page Title:</strong></td>
						<td><?php echo esc_html( $item->tittle ); ?></td>
					</tr>
					<tr>
						<td class="kbucket-text-right"><strong>Page Tags:</strong></td>
						<td><?php echo esc_html( $item->tags ); ?></td>
					</tr>
					<tr>
						<td class="kbucket-text-right"><strong>Page URL:</strong></td>
						<td><a href="<?php echo esc_attr( $item->link ); ?>" target="_blank"><?php echo esc_html( $item->link ); ?></a></td>
					</tr>
					<tr>
						<td class="kbucket-text-right"><strong>Page Author:</strong></td>
						<td><?php echo esc_html( $item->author ); ?></td>
					</tr>
					<tr>
						<td class="kbucket-text-right"><strong>Twitter Handle:</strong></td>
						<td><?php echo esc_html( $item->twitter ); ?></td>
					</tr>
					<tr>
						<td class="kbucket-text-right"><strong>Facebook Page:</strong></td>
						<td><?php echo esc_html( $item->facebook ); ?></td>
					</tr>
					<tr>
						<td class="kbucket-text-right"><strong>Comment:</strong></td>
						<td><?php echo esc_html( $item->description ); ?></td>
					</tr>
					<tr>
					<td colspan=2 style="border-bottom:2px solid pink; width:100%; min-width:700px; text-align:right;">
						<a href="javascript:void(0);" onclick="confirmdelete(<?php echo esc_js( $item->id_sug ); ?>)">Delete</a>
					</td>
					</tr>
					<?php endforeach ?>
				</table>
			</div>
		</div>

		<div id="country6" class="tabcontent">
			<?php
			$categories = $this->get_subcategories( 0, '`id_cat`,`name`' );
			?>
			<select name="category_id" id="category-dropdown">
				<option value="">Select Category</option>
				<?php foreach ( $categories as $c ): ?>
				<option value="<?php echo esc_attr( $c['id_cat'] ); ?>"><?php echo esc_html( $c['name'] ); ?></option>
				<?php endforeach; ?>
			</select>
			<select name="subcategory_id" id="subcategory-dropdown">
				<option value="">Select Subcategory</option>
			</select>
			<button id="button-save-sticky" class="button-secondary">Save changes</button>
			<div id="kbuckets"></div>
		</div>
			
		<div id="country7" class="tabcontent">
			<form method="post" id="form">
				<div style="width:700px;height:200px; margin-top:10px;">
					Ad code:<br/>
					<textarea name="kb-ad-code"></textarea><br/>
					<input type="submit" name="submit" value="Save Ad code"/>
				</div>
			</form>
		</div>
	</div>
	<?php
	}

	/**
	 * Render list of Categories for admin "Set Social" section
	 * @TODO must be optimized
	 * @return bool|string
	 */
	private function get_categories_dropdown() {

		$return_text = '';

		$categories = $this->get_subcategories( 0 );
		if ( is_array( $categories ) && count( $categories ) > 0 ) {
			foreach ( $categories as $category ) {
				$id_category = $category['id_cat'];
				$category_name = $category['name'];

				$return_text .= '<tr>
					<th colspan="3" class="kbucket-text-left-h40">' . ucfirst( $category_name ) . '</th>
					</tr>';
				$categories2 = $this->get_subcategories( $id_category );

				foreach ( $categories2 as $category2 ) {

					$return_text .= '<tr><td>----' . ucfirst( $category2['name'] ) . '</td>
									<td><input type="text" name="description[]" value="' . $category2['description'] . '"/>
									<input type="hidden" name="idArr[]" value="' . $category2['id_cat'] . '" />
									</td>
								<td><input type="file" name="upload_file[]"/></td>';
					if ( ! empty( $category2['image'] ) ) {
						$return_text .= '<td><img width="40" height="40" src="' . WPKB_PLUGIN_URL . '/uploads/categories/' . $category2['image'] . '"></td>';
					}
					$return_text .= '</tr>';
				}
			}
		} else {
			return false;
		}

		return $return_text;
	}

	/**
	 * Execute cURL request
	 * @param $url
	 * @return mixed
	 */
	public function curl_get_result( $url ) {
		$ch = curl_init();
		$timeout = 5;
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
		$data = curl_exec( $ch );
		curl_close( $ch );

		return $data;
	}

	/**
	 * Check if current page is Kbucket listings page
	 * @return bool
	 */
	private function is_kbucket_page()
	{
		global $wp;

		$slugsArr = explode( '/', $wp->request );

		$kbucketSlug = array_shift( $slugsArr );

		if ( !empty( $kbucketSlug ) && 'kbucket' == $kbucketSlug ) {
			return true;
		}

		return false;
	}
}

/**
 * Run install scripts by plugin activation
 * Create Kbucket tables and insert default settings
 * Fired by register_activation_hook
 */
function register_activation_kbucket() {

	/** @var $wpdb object */
	global $wpdb;

	$charsetCollate = $wpdb->get_charset_collate();

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	$sql = '
			CREATE TABLE IF NOT EXISTS ' . $wpdb->prefix . "kbucket (
				id_kbucket varchar(100) NOT NULL,
				id_cat varchar(100) DEFAULT NULL,
				title text,
				description text NOT NULL,
				link text,
				author varchar(50) DEFAULT NULL,
				author_alias varchar(50) DEFAULT NULL,
				facebook varchar(200) NOT NULL,
				twitter varchar(200) NOT NULL,
				pub_date varchar(100) DEFAULT NULL,
				add_date date DEFAULT NULL,
				status enum( '0','1' ) DEFAULT '1',
				image_url varchar(255) DEFAULT '',
				short_url varchar(255) DEFAULT '',
				post_id int(11) DEFAULT NULL,
				url_kbucket varchar(255),
			PRIMARY KEY  (id_kbucket),
			UNIQUE KEY id_kbucket_id_cat_index (id_kbucket,id_cat)
			) ENGINE=MyISAM $charsetCollate";
	dbDelta( $sql );

	$sql = 'CREATE TABLE IF NOT EXISTS ' . $wpdb->prefix . "kb_category (
				id_cat varchar(100) NOT NULL,
				name varchar(50) DEFAULT NULL,
				description text,
				image text,
				parent_cat varchar(100) DEFAULT '0',
				level bigint(3) DEFAULT NULL,
				add_date date DEFAULT NULL,
				status enum( '0','1' ) DEFAULT '1',
			PRIMARY KEY  (id_cat),
			UNIQUE KEY id_cat (id_cat,name,parent_cat)
			) ENGINE=MyISAM $charsetCollate";
	dbDelta( $sql );

	$sql = 'CREATE TABLE IF NOT EXISTS ' . $wpdb->prefix . "kb_page_settings (
				id_page int(3) NOT NULL AUTO_INCREMENT,
				sort_by varchar(10) DEFAULT NULL,
				sort_order varchar(10) DEFAULT NULL,
				no_listing_perpage int(4) DEFAULT NULL,
				main_no_tags int(4) DEFAULT NULL,
				author_no_tags int(4) DEFAULT NULL,
				related_no_tags int(4) DEFAULT NULL,
				main_tag_color1 varchar(10) DEFAULT NULL,
				main_tag_color2 varchar(10) DEFAULT NULL,
				author_tag_color1 varchar(10) DEFAULT NULL,
				author_tag_color2 varchar(10) DEFAULT NULL,
				related_tag_color1 varchar(10) DEFAULT NULL,
				related_tag_color2 varchar(10) DEFAULT NULL,
				author_tag_display enum( '0','1' ) DEFAULT '1',
				header_color varchar(10) DEFAULT NULL,
				author_color varchar(10) DEFAULT NULL,
				content_color varchar(10) DEFAULT NULL,
				tag_color varchar(10) DEFAULT NULL,
				status enum( '0','1' ) DEFAULT '1',
				page_title varchar(50) DEFAULT NULL,
				site_search int(1) DEFAULT NULL,
				m1 int(4) DEFAULT NULL,
				m2 int(4) DEFAULT NULL,
				m3 int(4) DEFAULT NULL,
				m4 int(4) DEFAULT NULL,
				a1 int(4) DEFAULT NULL,
				a2 int(4) DEFAULT NULL,
				a3 int(4) DEFAULT NULL,
				a4 int(4) DEFAULT NULL,
				r1 int(4) DEFAULT NULL,
				r2 int(4) DEFAULT NULL,
				r3 int(4) DEFAULT NULL,
				r4 int(4) DEFAULT NULL,
				bitly_username varchar(255) NOT NULL,
				bitly_key varchar(255) NOT NULL,
				advertisiment_code varchar(255) NOT NULL,
			PRIMARY KEY  (id_page)
			) ENGINE=MyISAM $charsetCollate";
	dbDelta( $sql );

	$sql = 'CREATE TABLE IF NOT EXISTS ' . $wpdb->prefix . "kb_suggest (
				id_sug int(2) NOT NULL AUTO_INCREMENT,
				id_cat varchar(100) DEFAULT NULL,
				tittle varchar(50) DEFAULT NULL,
				description text NOT NULL,
				tags varchar(200) NOT NULL,
				add_date date DEFAULT NULL,
				author varchar(50) DEFAULT NULL,
				link text,
				twitter text,
				facebook text,
				status enum( '0','1' ) DEFAULT '1',
			PRIMARY KEY  (id_sug)
			) ENGINE=MyISAM $charsetCollate";
	dbDelta( $sql );

	$sql = 'CREATE TABLE IF NOT EXISTS ' . $wpdb->prefix . "kb_tags (
				id_ktags int(3) NOT NULL AUTO_INCREMENT,
				id_kbucket varchar(100) DEFAULT NULL,
				id_tag int(3) DEFAULT NULL,
				status enum( '0','1' ) DEFAULT '1',
			PRIMARY KEY  (id_ktags),
			UNIQUE KEY id_kbucket_id_tag_index (id_kbucket,id_tag)
			) ENGINE=MyISAM  $charsetCollate;";
	dbDelta( $sql );

	$sql = 'CREATE TABLE IF NOT EXISTS ' . $wpdb->prefix . "kb_tag_details (
				id_tag int(3) NOT NULL AUTO_INCREMENT,
				name varchar(50) DEFAULT NULL,
				status enum( '0','1' ) DEFAULT '1',
			PRIMARY KEY  (id_tag),
			UNIQUE KEY name_index (name)
			) ENGINE=MyISAM  $charsetCollate";
	dbDelta( $sql );

	//@TODO Shortener service should be specified via dashboard
	$wpdb->query('INSERT IGNORE
				INTO ' . $wpdb->prefix . "kb_page_settings
	 			VALUES (
					1,'add_date','desc','10',50,50,50,'#F67B1F','#000','#F67B1F','#000','#F67B1F','#000','1',
					'#1982D1','#F67B1F','#666666','#109CCB','1','',1,10,12,14,16,10,12,14,16,10,12,14,16,
					'o_3fanobfis0','R_d4112d6f69ad4bd8706925e11c892893',''
				)
			");

	$kbucketPageExists = get_posts(
	 array(
		'name' => 'kbucket',
		'post_type' => 'page',
	 )
	);

	if ( ! $kbucketPageExists ) {
		$kbucketPage = array(
			'post_name' => 'kbucket',
			'post_title' => 'Kbucket',
			'post_status' => 'publish',
			'post_type' => 'page',
		);

		wp_insert_post( $kbucketPage );
	}

	add_option( 'kbucket_data', 'Default', '', 'yes' );
}

/**
 * Remove plugin data if plugin has been deleted
 * Fired by register_deactivation_hook
 */
function register_deactivation_kbucket() {

	if ( ! current_user_can( 'activate_plugins' ) ) {
		return;
	}

	//check_admin_referer( 'bulk-plugins' );

	// Important: Check if the file is the one
	// that was registered during the uninstall hook.
	/*if ( __FILE__ != WP_UNINSTALL_PLUGIN ) {
		//return;
	}*/

	/** @var $wpdb object */
	global $wpdb;

	$dropTables = array(
		$wpdb->prefix . 'kbucket',
		$wpdb->prefix . 'kb_category',
		$wpdb->prefix . 'kb_suggest',
		$wpdb->prefix . 'kb_tags',
		$wpdb->prefix . 'kb_tag_details',
		$wpdb->prefix . 'kb_page_settings',
	);

	$dropTablesStr = '' . implode( '`,`', $dropTables ) . '`';
	$wpdb->query( "DROP TABLE $dropTablesStr" );
}

/**
 * Chech if current page is home index page
 * @return bool
 */
function is_main_page()
{
	return is_home() || is_page( 'Home' ) ? true : false;
}

/**
 * @param $wp_rewrite
 */
function rewrite_rules( $wp_rewrite ) {

	$kbucketRules = array(
		'^kbucket/(.*)' => 'index.php?pagename=kbucket',
	);

	$wp_rewrite->rules = $kbucketRules + $wp_rewrite->rules;
}

/**
 * Array debbuging
 * @param $arr
 */
function dump( $arr ) {
	echo '<pre>';
	print_r( $arr );
	echo '</pre>';
}