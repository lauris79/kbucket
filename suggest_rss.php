<?php 
include_once('../../../wp-config.php');
global $wpdb;
$pid = $wpdb->get_var("select ID from wp_posts where post_content like '%[KBucket-Page]%' and post_status = 'publish' ");
$url = get_permalink($pid);
$data = $wpdb->get_results("
	SELECT a.*,b.* ,DATE_FORMAT(a.add_date,'%d-%m-%Y') as pubdate
	FROM wp_kb_suggest a 
	left join
	(
		SELECT c.id_cat as parid, b.id_cat as subid,c.name as parcat,b.name as  subcat,b.description as subdes, c.description as pades
		FROM wp_kb_category b
		inner join wp_kb_category c on c.id_cat = b.parent_cat
	) b on a.id_cat = b.subid
	");

function RFC2822($date, $time = '00:00') 
		{
    		list($d, $m, $y) = explode('-', $date);
    		list($h, $i) = explode(':', $time);
		    return date('r', mktime($h,$i,0,$m,$d,$y));
		}

header("Content-type: text/xml"); ?>
<?php echo '<?xml version="1.0" encoding="iso-8859-1"?>';?>
<rss version="2.0">
<channel>
  <title><![CDATA[Suggested Page URLs]]></title>
  <link><![CDATA[<?php echo $url;?>]]></link>
  <pubDate><?php echo RFC2822(date('d-m-Y'));?></pubDate>
  <?php foreach($data  as $item) {?>
  <item>
    <title><![CDATA[<?php echo $item->tittle;?>]]></title>
	<author><![CDATA[<?php echo $item->author;?>]]></author>
    <pubDate><![CDATA[<?php echo  $item->add_date;?>]]></pubDate>
    <link><![CDATA[<?php echo $item->link;?>]]></link>
    <description><![CDATA[<?php echo $item->description;?>]]></description>
	<category><![CDATA[<?php echo $item->tags;?>]]></category>
	<twitter_handle><![CDATA[<?php echo $item->twitter;?>]]></twitter_handle>
	<facebook_page><![CDATA[<?php echo $item->facebook;?>]]></facebook_page>
	</item>
	<?php }?>
  
</channel>
</rss>